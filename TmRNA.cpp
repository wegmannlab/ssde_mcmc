/*
 * TGene.cpp
 *
 *  Created on: Apr 22, 2014
 *      Author: wegmannd
 */

#include "TmRNA.h"

#include <math.h>

//--------------------------------------------------------------------------------
//TBaseInteraction
//--------------------------------------------------------------------------------
void TBaseInteraction::registerParameters(TParamVector* paramVec){
	paramVec->addParam(&c);
}

//--------------------------------------------------------------------------------
//TGeneInteraction
//--------------------------------------------------------------------------------
TGeneInteraction::TGeneInteraction(TmRNA* InteractingGene){
	type = "Gene";
	interactingGene = InteractingGene;
	interactingConcentrations = interactingGene->mRNAConcentrations;
}

TGeneInteraction::TGeneInteraction(std::string geneName, TmRNA* InteractingGene, std::string & paramC){
	type = "Gene";
	interactingGene = InteractingGene;
	interactingConcentrations = interactingGene->mRNAConcentrations;
	c.init(geneName+"."+InteractingGene->name+".c", paramC);
}

TBaseInteraction* TGeneInteraction::createCopy(std::vector<TmRNA*> & genes, TClock* Clock){
	//find interacting gene
	TmRNA* InteractingGene;
	bool found = false;
	for(std::vector<TmRNA*>::iterator it=genes.begin(); it!=genes.end(); ++it){
		if((*it)->name == interactingGene->name){
			InteractingGene = (*it);
			found = true;
			break;
		}
	}
	if(!found)  throw "Can not create copy of TGeneInteraction, gene '" + interactingGene->name + "' does not exist!";

	//create a new object as a copy and return pointer
	TGeneInteraction* copy = new TGeneInteraction(InteractingGene);
	copy->c.init(&c);
	return copy;
}

double TGeneInteraction::getValueNow(){
	return c.currentValue() * interactingGene->getDelayedConcentration();
}
double TGeneInteraction::getValueNowPlusHalfDeltaT(){
	return c.currentValue() * interactingGene->getDelayedPlusHalfDeltaTConcentration();
}
double TGeneInteraction::getValueNowPlusDeltaT(){
	return c.currentValue() * interactingGene->getDelayedPlusDeltaTConcentration();
}

std::string TGeneInteraction::getString(){
	return c.getString() + " * " + interactingGene->getDelayedConcentrationString();
}
void TGeneInteraction::writeCurValuesToInputFile(std::ofstream & out){
	out << interactingGene->name << "\t" << c.curVal << "\n";
}

//--------------------------------------------------------------------------------
//TLight
//--------------------------------------------------------------------------------
TLight::TLight(std::string geneName, TClock* Clock, std::string & paramC){
	type = "light";
	myClock = Clock;
	c.init(geneName+".LIGHT.c", paramC);
}

TLight::TLight(TClock* Clock){
	type = "light";
	myClock = Clock;
}

TBaseInteraction* TLight::createCopy(std::vector<TmRNA*> & genes, TClock* Clock){
	//create a new object as a copy and return pointer
	TLight* copy = new TLight(Clock);
	copy->c.init(&c);
	return copy;
}

double TLight::getValueNow(){
	if(myClock->isLight()) return c.currentValue();
	else return 0.0;
}

double TLight::getValueNowPlusHalfDeltaT(){
	if(myClock->isLight(myClock->timeOfDayPlusHalfDeltaT)) return c.currentValue();
	else return 0.0;
}

double TLight::getValueNowPlusDeltaT(){
	if(myClock->isLight(myClock->timeOfDayPlusDeltaT)) return c.currentValue();
	else return 0.0;
}

std::string TLight::getString(){
	return "Light";
}

void TLight::writeCurValuesToInputFile(std::ofstream & out){
	out << "LIGHT\t" << c.curVal << "\n";
}

//--------------------------------------------------------------------------------
//TmRNA
//--------------------------------------------------------------------------------
TmRNA::TmRNA(std::string Name, std::string BaselineProduction, std::string MaxProduction, std::string Degradation, std::string Delay, double HistoricalConcentration, TClock* Clock){
	name=Name;
	myClock=Clock;
	baseProduction.init(name + ".baseProduction", BaselineProduction);
	maxProduction.init(name + ".maxProduction", MaxProduction);
	degradation.init(name + ".degradation", Degradation);
	degradation.setMinMaxExtremes(-100.0, 1.0/Clock->deltaT);
	delay.init(name + ".delay", Delay, 0.0, 24.0);
	if(delay.currentValue()<=0.0) throw "Can not initialize gene '" + name + "' with a delay <=0!";
	mRNAConcentrations = NULL;
	historicalConcentration=HistoricalConcentration;
	delayInDeltaT=0;
	mRNAConcentrationsInitialized = false;
	initRKParams();
	hasInteractions = false;
}

TmRNA::TmRNA(TmRNA* other, TClock* Clock){
	//copy constructor
	//IT DOES NOT COPY INTERACTIONS!!
	name = other->name;
	myClock = Clock;
	baseProduction.init(&other->baseProduction);
	maxProduction.init(&other->maxProduction);
	degradation.init(&other->degradation);
	delay.init(&other->delay);
	if(delay.currentValue()<=0.0) throw "Can not initialize gene '" + name + "' with a delay <=0!";
	historicalConcentration = other->historicalConcentration;
	delayInDeltaT = other->delayInDeltaT;

	//RK params
	initRKParams();

	//concentrations
	mRNAConcentrationsInitialized = other->mRNAConcentrationsInitialized;
	if(mRNAConcentrationsInitialized){
		mRNAConcentrations = new TConcentrationsWithDelay(other->mRNAConcentrations, myClock);
	} else mRNAConcentrations = NULL;

	//Interactions
	hasInteractions = false;
}

void TmRNA::initRKParams(){
	k1 = 0;
	k2 = 0;
	k3 = 0;
	k4 = 0;
	oneSixth = 1.0/6.0;
	oneThird = 1.0/3.0;
}

void TmRNA::addInteraction(TmRNA* InteractingGene, std::vector<std::string> & line){
	//what type of interaction? If interacting gene is LIGHT, then create light interaction
	if(line[1]=="LIGHT")
		interactions.push_back(new TLight(name, myClock, line[2]));
	else
		interactions.push_back(new TGeneInteraction(name, InteractingGene, line[2]));

	hasInteractions = true;
}

void TmRNA::copyInteractions(TmRNA* other, std::vector<TmRNA*> & genes){
	//Interactions
	hasInteractions = other->hasInteractions;
	if(hasInteractions){
		for(interactionIt=other->interactions.begin(); interactionIt!=other->interactions.end(); ++interactionIt){
			interactions.push_back((*interactionIt)->createCopy(genes, myClock));
		}
	}
}

bool TmRNA::checkParameters(){
	if(baseProduction.currentValue() < 0.0) return false;
	if(maxProduction.currentValue() < baseProduction.currentValue()){
		return false;
	}
	if(degradation.currentValue() < 0.0){
		return false;
	}
	if(degradation.currentValue() * myClock->deltaT > 0.5){
		return false;
	}
	return true;
}

void TmRNA::addmRNAData(double time, double value){
	if(!mRNAConcentrationsInitialized) throw "Can not add data: concentrations have not yet been initialized!";
	mRNAConcentrations->addData(time, value);
}

bool TmRNA::hasData(){
	if(!mRNAConcentrationsInitialized) return false;
	else return mRNAConcentrations->hasData;
};

void TmRNA::registerParameters(TParamVector* paramVec){
	paramVec->addParam(&delay);
	paramVec->addParam(&degradation);
	paramVec->addParam(&baseProduction);
	paramVec->addParam(&maxProduction);
	paramVec->addRuleFirstLargerSecond(&maxProduction, &baseProduction);
	for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
		(*interactionIt)->registerParameters(paramVec);
	}
}

void TmRNA::initializeRungeKutta(){
	delay.setMinMax(myClock->deltaT*2.0, 24.0);
	delayInDeltaT = delay.currentValue() / myClock->deltaT;
	if(delayInDeltaT <= 1) throw "Can not initialize RungeKutta in gene '" + name + "' with deltaT=" + toString(delayInDeltaT) + ": delay is less than two deltaT units!";
	if(delayInDeltaT >= myClock->numTimeSteps){
		throw "Can not initialize RungeKutta in gene '" + name + "' with deltaT=" + toString(myClock->deltaT) + ": delay is more than deltaT units per day!";
	}
	if(mRNAConcentrationsInitialized){
		mRNAConcentrations->reset(delayInDeltaT, historicalConcentration);
	} else {
		mRNAConcentrations = new TConcentrationsWithDelay(myClock, delayInDeltaT, historicalConcentration);
		mRNAConcentrationsInitialized=true;
	}
}

double TmRNA::gompertz(double & A, double & minusB, double & minusValue){
	//return baselineProduction.currentValue() / (1.0 + exp(-val));
	return A * exp(minusB * exp(minusValue));
}
double TmRNA::gompertzAtZero(double & A, double & minusB){
	//return baselineProduction.currentValue() / (1.0 + exp(-val));
	return A * exp(minusB);
}

void TmRNA::updateConcentration(){
	//update k1, k2, k3, k4 and conc at once -> possible due to delays!
	//K1
	//k1 = myClock->deltaT * getSlopeNow();
	double curConcRNA = mRNAConcentrations->current();
	double deltaT = myClock->deltaT;
	double degrad = degradation.currentValue();
	double A = maxProduction.currentValue();
	double minusB = -log(A / baseProduction.currentValue());
	if(hasInteractions){
		double minusSum_c = 0.0;
		double slope;

		for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
			minusSum_c -= (*interactionIt)->getValueNow();
		}

		slope = gompertz(A, minusB, minusSum_c);

		k1 = deltaT * (slope - degrad * curConcRNA);

		minusSum_c = 0.0;
		for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
			minusSum_c -= (*interactionIt)->getValueNowPlusHalfDeltaT();
		}
		slope = gompertz(A, minusB, minusSum_c);
		k2 = deltaT * (slope - degrad * (curConcRNA + 0.5 * k1));
		k3 = deltaT * (slope - degrad * (curConcRNA + 0.5 * k2));

		minusSum_c = 0.0;
		for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
			minusSum_c -= (*interactionIt)->getValueNowPlusDeltaT();
		}
		slope = gompertz(A, minusB, minusSum_c);
		k4 = deltaT * (slope - degrad * (curConcRNA + k3));
	} else {
		double slope = gompertzAtZero(A, minusB);
		k1 = deltaT * (slope - degrad * curConcRNA);
		k2 = deltaT * (slope - degrad * (curConcRNA + 0.5 * k1));
		k3 = deltaT * (slope - degrad * (curConcRNA + 0.5 * k2));
		k4 = deltaT * (slope - degrad * (curConcRNA + k3));
	}

	mRNAConcentrations->setNext(curConcRNA + oneSixth*(k1+k4) + oneThird*(k2 + k3));
}

double TmRNA::updateConcentrationAndReturnDiff(){
    double nextOld = mRNAConcentrations->next();
    updateConcentration();
    return fabs(log(mRNAConcentrations->next()) - log(nextOld));
}

double TmRNA::getAmplitude(){
	return mRNAConcentrations->getAmplitude();
}

double TmRNA::getDelayedConcentration(){
	return mRNAConcentrations->delay();
}
double TmRNA::getDelayedPlusHalfDeltaTConcentration(){
	return mRNAConcentrations->delayPlusHalf();
}
double TmRNA::getDelayedPlusDeltaTConcentration(){
	return mRNAConcentrations->delayPlusOne();
}
double TmRNA::getConcentration(int bin){
	return mRNAConcentrations->at(bin);
}

double TmRNA::getLogLikelihoodTermForHastings(double & logGamma){
	return mRNAConcentrations->getLogLikelihoodTermForHastings(logGamma);
}

double TmRNA::getScaleEstimate(){
	return mRNAConcentrations->getScaleEstimate();
}
void TmRNA::print(TLog* logfile){
	logfile->startIndent("Interactions of gene '" + name + "':");
	bool first=true;
	std::string op="";
	for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
		if(first) first=false;
		else op= "* ";
		logfile->listFlush(op+(*interactionIt)->getString(), ' '); logfile->newLine();
	}
	//add degradation
	logfile->listFlush("- " + degradation.getString() + "*[" + name + "]", ' '); logfile->newLine();
	logfile->endIndent();
}

std::string TmRNA::getDelayedConcentrationString(){
	return "[" + name + "]_" + toString(delay.currentValue());
}

void TmRNA::simulateData(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma){
	mRNAConcentrations->simulateData(RandomGenerator, sigma, logGamma);
}

void TmRNA::simulateDataAdd(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma){
	mRNAConcentrations->simulateDataAdd(RandomGenerator, sigma, logGamma);
}

void TmRNA::writeObservedData(std::ofstream & out, double & numDays){
	mRNAConcentrations->writeObservedData(out, name, numDays, "m");
}

void TmRNA::writeCurValuesToInputFile(std::ofstream & out){
	//print Name, degradation, delay and historicalConcentration
	out << name << "\t" << baseProduction.curVal << "\t" << maxProduction.curVal << "\t" << degradation.curVal << "\t" << "-" << "\t" << delay.curVal << "\t" << historicalConcentration << "\n";
}

void TmRNA::writeInteractionsToInputFile(std::ofstream & out){
	for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
		out << name << "\t";
		(*interactionIt)->writeCurValuesToInputFile(out);
	}
}

void TmRNA::writeLogConcentrationHeader(std::ofstream & out){
	out << "\t" << name;
}

void TmRNA::writeLogConcentration(std::ofstream & out, long & bin, double & logGamma){
	out << "\t" << mRNAConcentrations->atLogScaled(bin, logGamma);
}

//--------------------------------------------------------------------------------
//TmRNAWithPreRNA
//--------------------------------------------------------------------------------
TmRNAWithPreRNA::TmRNAWithPreRNA(std::string Name, std::string BaselineProduction, std::string MaxProduction, std::string Degradation, std::string RnaProcessing, std::string Delay, double HistoricalConcentration, TClock* Clock):TmRNA(Name, BaselineProduction, MaxProduction, Degradation, Delay, HistoricalConcentration, Clock){
	RNAProcessing.init(name + ".RNAProcessing", RnaProcessing);
	RNAProcessing.setMinMaxExtremes(-10.0, 1.0/Clock->deltaT);
	preRNAConcentrationsInitialized = false;
	preRNAConcentrations = NULL;
	initRKParamsWithpreRNA();
}

TmRNAWithPreRNA::TmRNAWithPreRNA(TmRNAWithPreRNA* other, TClock* Clock):TmRNA(other, Clock){
	RNAProcessing.init(&other->RNAProcessing);

	//concentrations
	preRNAConcentrationsInitialized = other->preRNAConcentrationsInitialized;
	if(preRNAConcentrationsInitialized){
		preRNAConcentrations = new TConcentrations(other->preRNAConcentrations, myClock);
	} else preRNAConcentrations = NULL;

	initRKParamsWithpreRNA();
}

bool TmRNAWithPreRNA::checkParameters(){
	if(RNAProcessing.currentValue() < 0.0){
		return false;
	}
	if(RNAProcessing.currentValue() * myClock->deltaT > 0.5){
		return false;
	}
	return TmRNA::checkParameters();
}

void TmRNAWithPreRNA::initRKParamsWithpreRNA(){
	k1mRNA = 0.0;
	k2mRNA = 0.0;
	k3mRNA = 0.0;
	k4mRNA = 0.0;
}


void TmRNAWithPreRNA::initializeRungeKutta(){
	TmRNA::initializeRungeKutta();
	if(preRNAConcentrationsInitialized){
		preRNAConcentrations->reset(historicalConcentration);
	} else {
		preRNAConcentrations = new TConcentrations(myClock, historicalConcentration);
		preRNAConcentrationsInitialized = true;
	}
}

//SZS
//Bug hunting
double TmRNA::getmRNAConcentration()
{
	return mRNAConcentrations->current();
}
double TmRNA::getPreRNAConcentration()
{
	return -1.00;
}
//END_SZS

void TmRNAWithPreRNA::updateConcentration(){
	//update k1, k2, k3, k4 and conc at once -> possible due to delays!
	//K1
	//k1 = myClock->deltaT * getSlopeNow();
	double curConcRNA = mRNAConcentrations->current();
	double curConcPreRNA = preRNAConcentrations->current();
	double deltaT = myClock->deltaT;
	double degrad = degradation.currentValue();
	double rnaProcessing = RNAProcessing.currentValue();
	double A = maxProduction.currentValue();
	double minusB = -log(A / baseProduction.currentValue());

	//Update preRNA
	if(hasInteractions){
		double minusSum_c = 0.0;
		double slope;
		for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
			minusSum_c -= (*interactionIt)->getValueNow();
		}
		slope = gompertz(A, minusB, minusSum_c);

		k1 = deltaT * (slope - rnaProcessing * curConcPreRNA);

		minusSum_c = 0.0;
		for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
			minusSum_c -= (*interactionIt)->getValueNowPlusHalfDeltaT();
		}

		slope = gompertz(A, minusB, minusSum_c);
		k2 = deltaT * (slope - rnaProcessing * (curConcPreRNA + 0.5 * k1));
		k3 = deltaT * (slope - rnaProcessing * (curConcPreRNA + 0.5 * k2));

		minusSum_c = 0.0;
		for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
			minusSum_c -= (*interactionIt)->getValueNowPlusDeltaT();
		}

		slope = gompertz(A, minusB, minusSum_c);
		k4 = deltaT * (slope - rnaProcessing * (curConcPreRNA + k3));

	} else {
		double slope = gompertzAtZero(A, minusB);
		k1 = deltaT * (slope - rnaProcessing * curConcPreRNA);
		k2 = deltaT * (slope - rnaProcessing * (curConcPreRNA + 0.5 * k1));
		k3 = deltaT * (slope - rnaProcessing * (curConcPreRNA + 0.5 * k2));
		k4 = deltaT * (slope - rnaProcessing * (curConcPreRNA + k3));
	}

	preRNAConcentrations->setNext(curConcPreRNA + oneSixth*(k1+k4) + oneThird*(k2 + k3));

	//update mRNA
	k1mRNA = deltaT * (rnaProcessing * curConcPreRNA - degrad * curConcRNA);
	k2mRNA = deltaT * (rnaProcessing * (curConcPreRNA + 0.5 * k1) - degrad * (curConcRNA + 0.5 * k1mRNA));
	k3mRNA = deltaT * (rnaProcessing * (curConcPreRNA + 0.5 * k2) - degrad * (curConcRNA + 0.5 * k2mRNA));
	k4mRNA = deltaT * (rnaProcessing * (curConcPreRNA + k3) - degrad * (curConcRNA + k3mRNA));

	mRNAConcentrations->setNext(curConcRNA + oneSixth*(k1mRNA+k4mRNA) + oneThird*(k2mRNA + k3mRNA));
}

double TmRNAWithPreRNA::updateConcentrationAndReturnDiff(){
    double nextOld = mRNAConcentrations->next();
    double nextOldPre = preRNAConcentrations->next();
    updateConcentration();
    return std::max(abs(mRNAConcentrations->next() - nextOld), abs(preRNAConcentrations->next() - nextOldPre));
}

double TmRNAWithPreRNA::getAmplitude(){
	return std::min(mRNAConcentrations->getAmplitude(), preRNAConcentrations->getAmplitude());
}

void TmRNAWithPreRNA::addpreRNAData(double time, double value){
	if(!preRNAConcentrationsInitialized) throw "Can not add data: preRNA concentrations have not yet been initialized!";
	preRNAConcentrations->addData(time, value);
}

bool TmRNAWithPreRNA::hasData(){
	if(mRNAConcentrationsInitialized && mRNAConcentrations->hasData && preRNAConcentrationsInitialized && preRNAConcentrations->hasData) return true;
	return false;
};

void TmRNAWithPreRNA::registerParameters(TParamVector* paramVec){
	TmRNA::registerParameters(paramVec);
	paramVec->addParam(&RNAProcessing);
}

double TmRNAWithPreRNA::getLogLikelihoodTermForHastings(double & logGamma){
	return TmRNA::getLogLikelihoodTermForHastings(logGamma) + preRNAConcentrations->getLogLikelihoodTermForHastings(logGamma);
}

double TmRNAWithPreRNA::getScaleEstimate(){
	double tmp = 0.0;
	int num = 0;
	if(mRNAConcentrationsInitialized && mRNAConcentrations->hasData){
		tmp += mRNAConcentrations->getScaleEstimate();
		++num;
	}
	if(preRNAConcentrationsInitialized && preRNAConcentrations->hasData){
		tmp += preRNAConcentrations->getScaleEstimate();
		++num;
	}
	return tmp / num;
}

void TmRNAWithPreRNA::print(TLog* logfile){
	logfile->startIndent("Interactions of gene '" + name + "':");
	bool first=true;
	std::string op="";
	logfile->listFlush("preRNA of " + name + ": " ); logfile->newLine();
	for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt){
		if(first) first=false;
		else op= "* ";
		logfile->listFlush(op+(*interactionIt)->getString(), ' '); logfile->newLine();
	}
	//add RNAProcessing
	logfile->listFlush("- " + RNAProcessing.getString() + "*[" + name + ".pre" + "]", ' '); logfile->newLine();
	logfile->listFlush("mRNA of " + name + ": " ); logfile->newLine();
	logfile->listFlush(RNAProcessing.getString() + "*[" + name + ".pre" + "]", ' '); logfile->newLine();
	logfile->listFlush("- " + degradation.getString() + "*[" + name +  "]", ' '); logfile->newLine();

	logfile->endIndent();
}

void TmRNAWithPreRNA::simulateData(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma){
	mRNAConcentrations->simulateData(RandomGenerator, sigma, logGamma);
	preRNAConcentrations->simulateData(RandomGenerator, sigma, logGamma);
}

void TmRNAWithPreRNA::simulateDataAdd(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma){
	mRNAConcentrations->simulateDataAdd(RandomGenerator, sigma, logGamma);
	preRNAConcentrations->simulateDataAdd(RandomGenerator, sigma, logGamma);
}

void TmRNAWithPreRNA::writeObservedData(std::ofstream & out, double & numDays){
	TmRNA::writeObservedData(out, numDays);
	preRNAConcentrations->writeObservedData(out, name, numDays, "p");
}

void TmRNAWithPreRNA::writeCurValuesToInputFile(std::ofstream & out){
	//print Name, degradation, RNAProcessing, delay and historicalConcentration
	out << name << "\t" << baseProduction.curVal << "\t" << maxProduction.curVal << "\t" << degradation.curVal << "\t" << RNAProcessing.curVal << "\t"<< delay.curVal << "\t" << historicalConcentration << "\n";
}

void TmRNAWithPreRNA::writeLogConcentrationHeader(std::ofstream & out){
	out << "\t" << name << ".pre\t" << name;
}

void TmRNAWithPreRNA::writeLogConcentration(std::ofstream & out, long & bin, double & logGamma){
	out << "\t" << preRNAConcentrations->atLogScaled(bin, logGamma) << "\t" << mRNAConcentrations->atLogScaled(bin, logGamma);
}

//SZS
double TmRNAWithPreRNA::getPreRNAConcentration()
{
	return preRNAConcentrations->current();
}
//END_SZS
