/*
 * TGeneVector.h
 *
 *  Created on: May 19, 2015
 *      Author: wegmannd
 */

#ifndef TGENEVECTOR_H_
#define TGENEVECTOR_H_

//SZS
//modified due Win platform compatibility
#if defined _WIN32
	// Windows stuff
#else
	#define FALSE 0
	#define TRUE !(FALSE)	
#endif
//END_SZS

#include "TmRNA.h"

class TGeneVector{

//SZS
protected:
	std::vector<TmRNA*> genes;
	std::vector<TmRNAWithPreRNA*> genesWithPreRNA;
	std::vector<TmRNA*> allGenes;
	std::vector<TmRNA*>::iterator geneIt;
	TParamVector paramVec;

	std::vector<int> days;
	TClock wallClock;
	int* daysDiff;
	double sigma, minusOneOverTwoSimgaSquared;

	//MCMC parameters
	double logGamma, newLogGamma;
	double oldLogPriorDens, newLogPriorDens;
	double oldLogPosteriorDens, newLogPosteriorDens;
	long numAccepted;
	int numParamsToUpdate;
	double* normalRand;
	double* unifRand;
	double hastingsLog;
	long numTried;
	double heat;
	double propGamma;

	//Runge Kutta
	//Looking for eronous values during Runge Kutta steps
	double testingExponent;
	double highestPossibleConcentration;
	double lowestPossibleConcentration;
	int occuranceOfTestingConcentration;		//it is a number of 2^n-1 !!!
	bool correctConcentrationValues;
//END_SZS

private:
	TLog* logFile;
	std::vector<TmRNAWithPreRNA*>::iterator genesWithPreRNAIt;
	std::vector<TConcentrations*> concentrations;
	std::vector<TConcentrations*>::iterator concIt;

	int numDataPoints;
	int numDays;
	bool multipleDays;

	//MCMC parameters
	bool MCMCinitialized;
	double propRange;
	double logFirstNormalDensityTerm;

	//simulation parameters
	std::ofstream simParamOut;
	bool simParamOutOpen;
	bool writeRungeKutta;
	int ParametersToSearch, parameterSetsRejected;
	double MaxEpsilon, minimalAmplitude;
	bool writeSimulationFiles;

public:
	TGeneVector(TLog* LogFile, TParameters* parameters);
	TGeneVector(TGeneVector* other);
	virtual ~TGeneVector(){
		delete[] daysDiff;
		if(MCMCinitialized){
			delete[] normalRand;
			delete[] unifRand;
		}
		for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
			delete *geneIt;
	};

	void readGenes(std::string & filename);
	void readInteractions(std::string & filename);
	void readData(std::string filename);
	TmRNA* getGeneFromName(std::string & name);
	TmRNAWithPreRNA* getGeneWithPreRNAFromName(std::string & name);

	//Runge-Kutta
	void resetRungeKutta();
	void updateOneTimestepRungeKutta();
	void runRungeKutta(int length, bool verbose=false);
	void runAndWriteRungeKutta(int length, std::string filename, bool verbose=false);
	bool runRungeKuttaAndCheck(double maxEpsilon);
	void writeRungeKuttaResults(std::string filename, int sparsity=1);
	
	//SZS
	void updateOneTimestepRungeKuttaWithTesting();
	//END_SZS

	//MCMC
	void initializeMCMC(TParameters* parameters);
	void writeMCMCHeader(std::ofstream & mcmcOut);
	void writeMCMCOutput(std::ofstream & mcmcOut);
	void prepareMCMCRun();

	//SZS
	void modifPrepareMCMCRun();
	bool hasNumAccepted(){if (numAccepted == 0) return FALSE; return TRUE;};
	void initNumAccepted() {numAccepted = 0;};
	//END_SZS
	
	bool checkParameters();
	void sampleRandomParameterValuesFromPrior(TRandomGenerator* randomGenerator);
	void checkStartingValues();
	double getAmplitude();
	double getCurLogPosteriorDensity(){return logFirstNormalDensityTerm + oldLogPosteriorDens;};

	double calcLogLikelihood(double & thisLogGamma);
	double getLogLikelihoodTermForHastings(double & thisLogGamma);
	double getLogLikelihoodTermForHastingsAllDays(double & thisLogGamma);
	double getLogLikelihoodTermForHastingsAllDaysOnlyGammaUpdate(double & thisLogGamma);
	double getScaleEstimate();
	void prepareMCMCStep(TRandomGenerator* randomGenerator);
	void performMCMCStep();
	void swap(TGeneVector* other);
	int getNumAccepted(){return numAccepted;};
	void resetNumAccepted(){numAccepted = 0;};
	double getAcceptaneRate(){return (double) numAccepted / (double) numTried;};
	double getCurLogGamma(){return logGamma;};
	void setHeat(double Heat){heat = Heat; paramVec.setRange(pow(propRange, heat));};
	double& getHeat(){return heat;};
	double& getCurLogPosteriorDensityForHastings(){return oldLogPosteriorDens;};

	//simulation
	void initializeSimulations(TParameters* parameters);
	void setTimepointsToSimulateData(std::vector<double> & times);
	void openSimulationOutput(std::string & paramFile);
	void simulateData(int index, std::string outname, TRandomGenerator* randomGenerator);
	virtual bool checkParameterStability(){
		throw "Wrong function is called...check the type of the data created!";
	}

	int getNumDataPoints(){return numDataPoints;};
};



#endif /* TGENEVECTOR_H_ */
