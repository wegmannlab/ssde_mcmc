/*
 * TSSDE_MCMC.h
 *
 *  Created on: Apr 22, 2014
 *      Author: wegmannd
 */

#ifndef TSSDE_MCMC_H_
#define TSSDE_MCMC_H_

#include "stringFunctions.h"
#include "TParameters.h"
#include "TRandomGenerator.h"

//SZS
#include "TExtGeneVector.h"
//END_SZS

#include <omp.h>

class TSSDE_MCMC{
public:
	TLog* logfile;
	TParameters* myParameters;
	TRandomGenerator* randomGenerator;
	int numCoresToUse; //for paralellization

	//constructors
	TSSDE_MCMC(TLog* Logfile, TParameters* TParameters);
	~TSSDE_MCMC(){
		delete randomGenerator;
	};

	//functions
	void performMCMCInference();
	void calcLikelihood();
	void simulate();
};


#endif /* TSSDE_MCMC_H_ */
