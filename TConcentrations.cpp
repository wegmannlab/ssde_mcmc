/*
 * TConcentrations.cpp
 *
 *  Created on: May 19, 2015
 *      Author: wegmannd
 */

#include "TConcentrations.h"


//--------------------------------------------------------------------------------
//TClock
//--------------------------------------------------------------------------------
TClock::TClock(){
	//initialize values
	deltaT = -1.0;
	hoursPerDay = 24.0;
	numTimeSteps = 0;
	ftp = 0.0;

	//set time of day and indexes
	reset();
}

void TClock::setStepsPerDay(long & stepsPerDay){
	numTimeSteps = stepsPerDay;
	deltaT = hoursPerDay / (double) numTimeSteps;
}

void TClock::setFtp(double Ftp){
	ftp = Ftp;
}

void TClock::reset(){
	timeOfDay = 0.0;
	timeOfDayPlusDeltaT = deltaT;
	timeOfDayPlusHalfDeltaT = deltaT/2.0;
	currentIndex = 0;
	nextIndex = 1;
	day = 0;
	timePassed = 0.0;
}

void TClock::increment(){
	//current index
	++currentIndex;
	if(currentIndex == numTimeSteps){
		currentIndex = 0;
		timeOfDay = 0;
		++day;
		timePassed = day * 24.0;
	} else {
		timeOfDay = deltaT*currentIndex;
		timePassed += deltaT;
	}
	timeOfDayPlusHalfDeltaT = timeOfDay + deltaT/2.0;

	//next index
	++nextIndex;
	if(nextIndex == numTimeSteps){
		nextIndex=0;
	}
	timeOfDayPlusDeltaT = deltaT * nextIndex;

	//increment concentrations
	for(conObjIt=concentrationObjects.begin(); conObjIt!=concentrationObjects.end(); ++conObjIt)
		(*conObjIt)->increment();
}

void TClock::addConcentrationObject(TConcentrationsWithDelay* ConcentrationObject){
	concentrationObjects.push_back(ConcentrationObject);
}

long TClock::getIndexFromTime(double & Time){
	return Time / deltaT;
}

bool TClock::isLight(){
	return isLight(timeOfDay);
}

bool TClock::isLight(double & time){
	return time < ftp;
}

//--------------------------------------------------------------------------------
//TConcentrations
//--------------------------------------------------------------------------------
TConcentrations::TConcentrations(TClock* Clock, const double & HistoricalConcentration){
	myClock = Clock;
	concentrations = new double[myClock->numTimeSteps];
	reset(HistoricalConcentration);
	hasData = false;
}

TConcentrations::TConcentrations(TConcentrations* other, TClock* Clock){
	myClock = Clock;

	//concentrations
	concentrations = new double[myClock->numTimeSteps];
	for(int i=0; i<myClock->numTimeSteps; ++i) concentrations[i] = other->concentrations[i];

	//data
	hasData = other->hasData;
	if(hasData){
		for(dataIt=other->observedData.begin(); dataIt!=other->observedData.end(); ++dataIt)
			observedData.push_back(*dataIt);
	}
}


void TConcentrations::reset(const double & HistoricalConcentration){
	for(int i=0; i<myClock->numTimeSteps; ++i) concentrations[i] = HistoricalConcentration;
}

void TConcentrations::addData(double & Time, double & Value){
	if(Time < 0.0) throw "Timepoint of data has to be >= 0.0!";
	if(Time >= 24.0) throw "Time of data cannot be >= 24.0!";
	if(Value < 0.0) throw "Concentration < 0.0!";
	observedData.push_back( std::pair<long, double>(myClock->getIndexFromTime(Time), log(Value)));
	hasData = true;
}

double TConcentrations::getLogLikelihoodTermForHastings(double & logGamma){
	//Normal distribution with mean = Runge-Kutta Concentration and std = sigma
	//However, the mean is further scaled by Gamma
	//For the hasting term, 1/(sigma*sqrt(2*pi)) cancels out
	//and the sigma term in the exponent can be taken in front of the sum over all data points
	if(!hasData) return 0.0;

	double ll = 0.0;
	double tmp;
	for(dataIt=observedData.begin(); dataIt!=observedData.end(); ++dataIt){
		tmp = dataIt->second - log(concentrations[dataIt->first]) - logGamma;
		ll +=	tmp * tmp;
	}
	return ll;
}

double TConcentrations::getScaleEstimate(){
	if(!hasData) return 0.0;
	//take average of log(Gamma)
	double scale = 1.0;
	for(dataIt=observedData.begin(); dataIt!=observedData.end(); ++dataIt){
		scale += dataIt->second - log(concentrations[dataIt->first]);
	}
	return scale / observedData.size();
}

void TConcentrations::simulateData(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma){
	for(dataIt=observedData.begin(); dataIt!=observedData.end(); ++dataIt){
		dataIt->second = RandomGenerator->getNormalRandom(log(concentrations[dataIt->first]) + logGamma, sigma);
	}
}

void TConcentrations::simulateDataAdd(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma){
	//add to data to create averages across multiple days
	for(dataIt=observedData.begin(); dataIt!=observedData.end(); ++dataIt){
		dataIt->second += RandomGenerator->getNormalRandom(log(concentrations[dataIt->first]) + logGamma, sigma);
	}
}

void TConcentrations::writeObservedData(std::ofstream & out, std::string & geneName, double & numDays, std::string type){
	for(dataIt=observedData.begin(); dataIt!=observedData.end(); ++dataIt){
		out << geneName << "\t" << (double) dataIt->first * myClock->deltaT << "\t" << type << "\t" << exp(dataIt->second)/numDays <<  "\n";
	}
}

double TConcentrations::getAmplitude(){
     double min = concentrations[0];
     double max = concentrations[0];
     for(int i=0; i<myClock->numTimeSteps; ++i){
         if(concentrations[i] < min) min = concentrations[i];
         if(concentrations[i] > max) max = concentrations[i];
     }
     return max - min;
}


//--------------------------------------------------------------------------------
//TConcentrationsWithDelay
//--------------------------------------------------------------------------------
TConcentrationsWithDelay::TConcentrationsWithDelay(TClock* Clock, const int & delayInDeltaT, const double & HistoricalConcentration):TConcentrations(Clock, HistoricalConcentration){
	myClock->addConcentrationObject(this);
	reset(delayInDeltaT, HistoricalConcentration);
}

TConcentrationsWithDelay::TConcentrationsWithDelay(TConcentrationsWithDelay* other, TClock* Clock):TConcentrations(other, Clock){
	myClock->addConcentrationObject(this);
	//delay
	delayIndex = other->delayIndex;
	delayPlusOneIndex = other->delayPlusOneIndex;
	delayedPlusHalfConcentration = other->delayedPlusHalfConcentration;

}

void TConcentrationsWithDelay::reset(const int & delayInDeltaT, const double & HistoricalConcentration){
	for(int i=0; i<myClock->numTimeSteps; ++i) concentrations[i] = HistoricalConcentration;
	delayedPlusHalfConcentration = HistoricalConcentration;
	delayIndex=myClock->numTimeSteps - delayInDeltaT;
	delayPlusOneIndex = delayIndex + 1;
	if(delayPlusOneIndex==myClock->numTimeSteps) delayPlusOneIndex = 0;
}

void TConcentrationsWithDelay::increment(){
	++delayIndex;
	if(delayIndex==myClock->numTimeSteps) delayIndex = 0;
	++delayPlusOneIndex;
	if(delayPlusOneIndex==myClock->numTimeSteps) delayPlusOneIndex=0;
	delayedPlusHalfConcentration = (concentrations[delayIndex] + concentrations[delayPlusOneIndex] ) / 2.0;
}


