//SZS
//2015.11.11

#ifndef TEXTGENEVECTOR_H_
#define TEXTGENEVECTOR_H_

#include "TGeneVector.h"
#include "string.h"

const double highTreshold[10]							= { 12.00, 7.50, 5.00, 3.50, 2.50, 2.35, 2.20, 2.00, 1.90, 1.80};
const double lowTreshold[10]							=  { 3.50, 2.50, 2.10, 1.50, 1.35, 1.25, 1.20, 1.18, 1.15, 1.10};
const double similarityThreshold[10]			= { 0.000000001, 0.00000001, 0.0000001, 0.000001, 0.00001, 0.0001, 0.0003, 0.001, 0.003, 0.01};

class TExtGeneVector : public TGeneVector
{
	private:
		//Input parameter values
		int executionMode;
		int expInterpolation;
		int rungeKuttaDaysTruncated;
		int coarseFineResolutionRatio;

		//Helper variables
		bool isFirstTimeExecuted;
		bool speedRoutineAllowed;

		//Task execution related parameters
		int nrOfAllGenes;
		int nrOfGenes;
		int nrOfGenesWithPreRNA;
		int numOfConcArrays;
		int maxExperimentSizeInDays;
		int maxStepsInOneDay;
		int sizeOfConcArray;
		int twiceSizeOfConcArray;
		double oneSixth, oneThird, twoThird;
		unsigned int minimalDelayInDeltaT;
		unsigned int maximalDelayInDeltaT;
		double deltaT;
		double halfDeltaT;
		int lightLimitInDeltaT;

		//MCMC related parameters
		double critValue;													//is calculated like LL-values
		double oldCritValue;
		double lastCritValueDiff;									//the difference from crit values in case of consecutive steps
		double lastAcceptedCritValue;
		int simulatedDays;												//the number of simulated days
		double highThrCritValue;									//the threshold level of critical value (decide in advance that one solution could be good enough or not)
		double scale;															//the new scale
		double ownLogPriorDens;										//the newLogPriorDens determined by new algorithms
		double ownLogPosteriorDens;								//the newLogPosteriorDens determined by new algorithms
		double ownLogGamma;												//the logGamma determined by new algorithms

		//Debug helper parameters
		int errors;
		double debugPropThreshold;

		//Elimination control of possible solutions
		int TotalCases;
		int RejectedCases;

		//Execution helper arrays

			//static
			int* geneType;													//array of gene types for each gene
			void** genePtr;													//pointer to each gene
			int* interactionsCounter;								//number of interactions for each gene
			int* interactionsArray;									//array of the interations
			int* arrayNrOfObs;											//array of the number of observation that each gene has for mRNA and preRNA
			int** obsTimePtr;												//array of concentration measurement times for each genes and concentration types
			double** obsConcPtr;										//array of measured concentrations for each genes and concentration types
			int maxObsTime;													//the time of the last measurement during a day

			//partially static
			double* concArray;											//array of concentrations

			//dynamic
			unsigned int* delayArrayInDeltaT;				//array of delays for each gene
			unsigned int* minimumDelayArrayInDeltaT;//array of minimal delays of interactions for each gene
			double * currentDegradation;						//array of degradation for each gene
			double* value_A;												//array of term A for each gene
			double* value_minusB;										//array of term minusB for each gene
			double* value_minusSumC;								//array of minusSumC for each gene
			double* value_RNAProcessing;						//array of RNAProcessing level for only genes with preRNA
			double* lastSumC;												//array of last minusSumC values
			double* lastExpSumC;										//array of last exp(minusSumC) values
			double* interactionsCurVal;							//array of curVal-s for each interaction of each 
			double* constSlope;											//array of slopes is in constant phase of simulation 
			double* darkConstSlope;									//array of dark slopes is in constant phase of simulation (light is off)
			double* lastSlope;											//array of last slopes
			double* arrayMinusSumC;									//temporal array of minusSumC-s
			double* arrayExpSumC;										//temporal array of exp(minusSumC)-s
			double* arrayOfSlopes;									//temporal array of slopes
			double* arrayOfMidSlopes;								//temporal array of mid slopes
			int* cyclesForGenes;										//helper array
			int* truncCyclesForGenes;								//helper array

	protected:
	public:
		TExtGeneVector(TLog* LogFile, TParameters* parameters);
		TExtGeneVector(TExtGeneVector* other);
		virtual ~TExtGeneVector(){freeArrays();};
		void initializeAdditionalParameters(TParameters* parameters);
		void setAndCheckStartingValues(TRandomGenerator* randomGenerator);
		void onlyCheckStartingValues();
		bool initializeAdditionalFields();
		void freeArrays();
		void prepareArraysForCalculations();
		void executeInteractionFreePhase();
		void executeInteractionFreePhaseForOneGene(int geneNr, int startPosition, int endPosition);
		void executeInteractionActivePhaseForOneGene(int geneNr, int startPosition, int endPosition);
		void executeInPresenceOfActiveInteractions();
		void smartExecuteInPresenceOfActiveInteractions();
		void calculateSquareDiffAndScaleFromObsData(int dayOfInvestigation);
		double calculateCritValue(int dayOfInvestigation, double LogGamma);
		void performOneMCMCStep();
		
		//Debug helper functions
		int runRungeKuttaUsingDebug(int length);
		double getLogLikelihoodTermForHastingsAllDaysUsingDebug(double & logGamma);
		bool checkStartingValuesUsingDebug();

};


#endif /* TEXTGENEVECTOR_H_ */
