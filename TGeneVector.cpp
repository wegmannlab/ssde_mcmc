/*
 * TGeneVector.cpp
 *
 *  Created on: May 19, 2015
 *      Author: wegmannd
 */

#include "TGeneVector.h"

//SZS
//created due Win platform compatibility SZS
	#if defined _WIN32
	  // Windows stuff
    #pragma warning(disable: 4800)
		#pragma warning(disable: 4804)
	#endif
//END_SZS

//--------------------------------------------------------------------------------
//TGeneVector
//--------------------------------------------------------------------------------
TGeneVector::TGeneVector(TLog* LogFile, TParameters* parameters){
	logFile=LogFile;
	MCMCinitialized = false;
	simParamOutOpen = false;
	writeRungeKutta = false;

	//read Runge Kutta paramters
	logFile->startIndent("Settings for the Runge-Kutta numerical evaluations:");
	//days at which likelihood is evaluated
	parameters->fillParameterIntoVector("RungeKuttaDays", days, ',');
	if(days.size()<1) throw "Runge Kutta days have to include at least one day!";
	logFile->list("Runge-Kutta will be run for " + concatenateString(days, ", ") + " days");
	//establish differences
	numDays = days.size();
	if(numDays>1) multipleDays = true;
	else multipleDays = false;
	daysDiff = new int[numDays];
	std::vector<int>::iterator it=days.begin();
	daysDiff[0] = *it; ++it;
	for(int i=1; i<numDays; ++it, ++i){
		if(*it < daysDiff[i-1]) throw "Runge-Kutta days are not in chronological order!";
		daysDiff[i] = (*it - daysDiff[i-1]);
	}
	long numSteps = parameters->getParameterLongWithDefault("RungeKuttaSteps", 1000);
	wallClock.setStepsPerDay(numSteps);
	logFile->list("Initializing Runge-Kutta with " +toString(numSteps) + " steps per day, implying a deltaT of " + toString(wallClock.deltaT));

	//sigma
	sigma = parameters->getParameterDoubleWithDefault("sigma",1.0);
	minusOneOverTwoSimgaSquared = -1.0 / (2.0 * sigma * sigma);
	logFile->list("Using a sampling standard deviation (sigma) of " + toString(sigma));
	logFile->endIndent();

	//read genes and interactions
	std::string geneFile = parameters->getParameterString("genes");
	std::string interactionFile = parameters->getParameterString("interactions");
	readGenes(geneFile);
	readInteractions(interactionFile);

	//set ftp
	wallClock.setFtp(parameters->getParameterDouble("ftp"));

	//register parameters
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
		(*geneIt)->registerParameters(&paramVec);
	numParamsToUpdate = paramVec.numParams();

	//steps per day -> initialize clock and genes!
	logFile->listFlush("Initializing Runge-Kutta in all genes ...");
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
		(*geneIt)->initializeRungeKutta();
	logFile->write(" done!");


	//MCMC variables
	numDataPoints = 0;
	logGamma = 0.0;
	newLogGamma = 0.0;
	logFirstNormalDensityTerm = 0.0;
	oldLogPriorDens = 0.0;
	newLogPriorDens = 0.0;
	oldLogPosteriorDens = 0.0;
	newLogPosteriorDens = 0.0;
	hastingsLog = 0.0;
	heat = 1.0;
	numAccepted = 0;
	propGamma = 0.0;
	propRange = 0.0;

	//SZS
	//RungeKutta steps checking (eliminate wrong concentration values)
	testingExponent = 69;
	highestPossibleConcentration = exp(testingExponent);
	testingExponent = 269;
	lowestPossibleConcentration = exp(-testingExponent);
	occuranceOfTestingConcentration = 15;
	//END_SZS
}

TGeneVector::TGeneVector(TGeneVector* other){
	//creates an unitinitialized copy of a TGeneVector
	//-> will copy genes, interactions, data, parameters and RK-settings
	//Will NOT copy simulation settings
	logFile = other->logFile;
	simParamOutOpen = false;
	writeRungeKutta = false;

	//copy genes and data
	wallClock.setStepsPerDay(other->wallClock.numTimeSteps);
	wallClock.setFtp(other->wallClock.ftp);
	for(geneIt=other->genes.begin(); geneIt!=other->genes.end(); ++geneIt){
		genes.push_back(new TmRNA(*geneIt, &wallClock));
		allGenes.push_back(*genes.rbegin());
	}
	for(genesWithPreRNAIt=other->genesWithPreRNA.begin(); genesWithPreRNAIt!=other->genesWithPreRNA.end(); ++genesWithPreRNAIt){
		genesWithPreRNA.push_back(new TmRNAWithPreRNA(*genesWithPreRNAIt, &wallClock));
		allGenes.push_back(*genesWithPreRNA.rbegin());
	}
	numDataPoints = other->numDataPoints;

	//copy interactions
	std::vector<TmRNA*>::iterator otherIt = other->allGenes.begin();
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt, ++otherIt)
		(*geneIt)->copyInteractions(*otherIt, allGenes);

	//register parameters
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
		(*geneIt)->registerParameters(&paramVec);
	numParamsToUpdate = paramVec.numParams();

	//days
	days = other->days;
	numDays = days.size();
	multipleDays = other->multipleDays;
	daysDiff = new int[numDays];
	daysDiff[0]=days[0];
	for(int i=1; i<numDays; ++i) daysDiff[i] = days[i] - daysDiff[i-1];

	//sigma
	sigma = other->sigma;
	minusOneOverTwoSimgaSquared = other->minusOneOverTwoSimgaSquared;

	//MCMC variables
	MCMCinitialized = other->MCMCinitialized;
	logGamma = other->logGamma;
	logFirstNormalDensityTerm = other->logFirstNormalDensityTerm;
	propGamma = other->propGamma;
	propRange = other->propRange;
	if(MCMCinitialized){
		normalRand = new double[numParamsToUpdate+1];
		unifRand = new double[numParamsToUpdate+1];
		paramVec.setRange(propRange);
	}

	//temporary MCMC variables
	oldLogPriorDens = other->oldLogPriorDens;
	oldLogPosteriorDens = other->oldLogPosteriorDens;
	newLogGamma = 0.0;
	newLogPriorDens = 0.0;
	newLogPosteriorDens = 0.0;
	hastingsLog = 0.0;
	heat = 1.0;
	numAccepted = 0;
	numTried = 0;

	//SZS
	//RungeKutta steps checking (eliminate wrong concentration values)
	testingExponent = 69;
	highestPossibleConcentration = exp(testingExponent);
	testingExponent = 269;
	lowestPossibleConcentration = exp(-testingExponent);
	occuranceOfTestingConcentration = 15;
	//END_SZS
}

void TGeneVector::readGenes(std::string & filename){
	//this function will read genes from a file
	logFile->listFlush("Reading genes from file '" + filename + "' ...");
	std::ifstream input(filename.c_str());
	if(!input) throw "File '" + filename +"' could not be opened!";

	//read header
	std::string line;
	std::getline(input, line);

	//parse line by line
	std::vector<std::string> vec;
	int lineNum=0;
	while(input.good() && !input.eof()){
		++lineNum;
		std::getline(input, line);
		line = extractBefore(line, "//");
		trimString(line);
		if(!line.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
			//we expect 7 columns: name, baseProduction, maxProduction, degradation, RNAProcessing, delay and hist. conc
			if(vec.size()!=7) throw "Wrong number of entries in file '" + filename + "' on line " + toString(lineNum) + ","
					" expect 7 (name, baseline production, max production, degradation, RNAProcessing, delay and historical concentration), but got " + toString(vec.size()) + "!";

			//check if name exists
			for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
				if((*geneIt)->name == vec[0]) throw "Gene '" + vec[0] + "' already exists!";
			}

			//push back
			if (vec[4] != "-") {
				genesWithPreRNA.push_back(new TmRNAWithPreRNA(vec[0], vec[1], vec[2], vec[3], vec[4], vec[5], stringToDoubleCheck(vec[6]), &wallClock));
				allGenes.push_back(*genesWithPreRNA.rbegin());
			} else {
				genes.push_back(new TmRNA(vec[0], vec[1], vec[2], vec[3], vec[5], stringToDoubleCheck(vec[6]), &wallClock));
				allGenes.push_back(*genes.rbegin());
			}
		}
	}

	logFile->write("done!");
	logFile->conclude("read " + toString(allGenes.size()) + " genes");
}

void TGeneVector::readInteractions(std::string & filename){
	//this function will read genes from a file
	logFile->listFlush("Reading gene interactions from file '" + filename + "' ...");
	std::ifstream input(filename.c_str());
	if(!input) throw "File '" + filename +"' could not be opened!";

	//read header
	std::string line;
	std::getline(input, line);

	//parse line by line
	std::vector<std::string> vec;
	int lineNum=0;
	TmRNA* affectedGene;
	TmRNA* interactingGene;

	while(input.good() && !input.eof()){
		++lineNum;
		std::getline(input, line);
		line=extractBefore(line, "//");
		trimString(line);
		if(!line.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
			if(vec.size() != 3)
				throw "Wrong number of entries! An interaction needs 3, not " + toString(line.size()) + " on line " + toString(lineNum) + "!";
			//first column refers to the gene that is affected by interactions
			affectedGene = getGeneFromName(vec[0]);
			//second column refers to interacting gene.
			interactingGene = getGeneFromName(vec[1]);
			affectedGene->addInteraction(interactingGene, vec);
		}
	}

	logFile->write("done!");
	logFile->addIndent(1);
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
		(*geneIt)->print(logFile);
	}
	logFile->endIndent();
}

void TGeneVector::readData(std::string filename){
	//this function will read genes from a file
	logFile->listFlush("Reading data from file '" + filename + "' ...");
	std::ifstream input(filename.c_str());
	if(!input) throw "File '" + filename +"' could not be opened!";

	//read header
	std::string line;
	std::getline(input, line);

	//parse line by line
	std::vector<std::string> vec;
	int lineNum = 0;
	numDataPoints = 0;
	TmRNA* gene;
	TmRNAWithPreRNA* geneWithPre;
	while(input.good() && !input.eof()){
		++lineNum;
		std::getline(input, line);
		line=extractBefore(line, "//");
		if(!line.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
			//we expect 4 columns: gene, time, interaction type and value
			if(vec.size()!=4) throw "Wrong number of entries in file '" + filename + "' on line " + toString(lineNum) + ","
					" expect 4 (gene name, time of day, interaction Type (m/p),  expression value), but got " + toString(vec.size()) + "!";

			//check type of data
			if(vec[2] == "m"){
				gene = getGeneFromName(vec[0]);
				gene->addmRNAData( stringToDoubleCheck(vec[1]), stringToDoubleCheck(vec[3]));
			} else if(vec[2] == "p"){
				geneWithPre = getGeneWithPreRNAFromName(vec[0]);
				geneWithPre->addpreRNAData(stringToDoubleCheck(vec[1]), stringToDoubleCheck(vec[3]));
			} else throw "Unknown data type " + vec[2] + "'!";
			++numDataPoints;
		}
	}
	logFile->write("done!");
	logFile->conclude("Added a total of ", numDataPoints, " data points");

	if(numDataPoints < 1) throw "No data has been provided!";
}

TmRNA* TGeneVector::getGeneFromName(std::string & name){
	//If tag "LIGHT" is used, it indicates an interaction with light.
	if(name=="LIGHT") return NULL; //needed as fail-save because the interaction with LIGHT is not a gene.
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
		if((*geneIt)->name == name) return *geneIt;
	}
	throw "Gene '" + name + "' does not exist!";
}

TmRNAWithPreRNA* TGeneVector::getGeneWithPreRNAFromName(std::string & name){
	for(genesWithPreRNAIt=genesWithPreRNA.begin(); genesWithPreRNAIt!=genesWithPreRNA.end(); ++genesWithPreRNAIt){
		if((*genesWithPreRNAIt)->name == name) return *genesWithPreRNAIt;
	}
	throw "Gene with preRNA '" + name + "' does not exist!";
}
//--------------------------------------------------------------
//Function to run Runge Kutta
//--------------------------------------------------------------
void TGeneVector::resetRungeKutta(){
	wallClock.reset();
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
		(*geneIt)->initializeRungeKutta();
}

void TGeneVector::updateOneTimestepRungeKutta(){
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
		(*geneIt)->updateConcentration();
	wallClock.increment();
}

//SZS
void TGeneVector::updateOneTimestepRungeKuttaWithTesting(){
	double concentration;
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
	{
		(*geneIt)->updateConcentration();
		
		//Test mRNA concentration
		concentration = (*geneIt)->mRNAConcentrations->current();

		//It seems to be strange, but the expression concentration != concentration is FALSE in case of concentration = NaN Not a number value!!!
		if ((concentration > highestPossibleConcentration) || (concentration != concentration)) correctConcentrationValues = FALSE;  
		else
			if ((concentration < lowestPossibleConcentration) && (concentration != -1.0)) correctConcentrationValues = FALSE;
		
		//Test preRNA concentration
		concentration = (*geneIt)->getPreRNAConcentration();
		if ((concentration > highestPossibleConcentration) || (concentration != concentration)) correctConcentrationValues = FALSE;  
		else
			if ((concentration < lowestPossibleConcentration) && (concentration != -1.0)) correctConcentrationValues = FALSE;
	}
	wallClock.increment();
}
//END_SZS

void TGeneVector::runRungeKutta(int length, bool verbose){
	long steps = wallClock.numTimeSteps * length - 1;
	if(verbose) logFile->listFlush("Running Runge-Kutta for ", steps, " steps ...");

	//SZS
	//modified due Win platform compatibility
	#if defined _WIN32
	  // Windows stuff
		struct timeval2 start, end;
	#else
	  // Other stuff
		struct timeval start, end;
	#endif
	//END_SZS
	
	gettimeofday(&start, NULL);

	//SZS
	correctConcentrationValues = TRUE;
  for(long i=0; i<steps; ++i){
		if ((i & occuranceOfTestingConcentration) != 0xf)
			updateOneTimestepRungeKutta();
		else
		{
			updateOneTimestepRungeKuttaWithTesting();
			if (correctConcentrationValues == FALSE) break;
		}
  }
	//END_SZS

	if(verbose){
		gettimeofday(&end, NULL);
		float runtime=(end.tv_sec  - start.tv_sec);
		logFile->write(" done (", runtime, "s)!");
	}
}

void TGeneVector::runAndWriteRungeKutta(int length, std::string filename, bool verbose){
	long steps = wallClock.numTimeSteps * length - 1;
	if(verbose) logFile->listFlush("Running Runge-Kutta for ", steps, " steps");

	//SZS
	//modified due Win platform compatibility
	#if defined _WIN32
	  // Windows stuff
		struct timeval2 start, end;
	#else
	  // Other stuff
		struct timeval start, end;
	#endif
	//END_SZS

  gettimeofday(&start, NULL);

  //open output
  std::ofstream out;
	out.open(filename.c_str());
	if(!out) throw "Output file '" + filename + "' could not be opened!";
	//write header and initial values
	out << "totTime\ttimeOfDay\tbin";
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
		(*geneIt)->writeLogConcentrationHeader(out);
	out << "\n";
	out << wallClock.timePassed << "\t" << wallClock.timeOfDay << "\t" << wallClock.currentIndex;
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
		(*geneIt)->writeLogConcentration(out, wallClock.currentIndex, logGamma);
	}
	out << "\n";

	for(long i=0; i<steps; ++i){
		updateOneTimestepRungeKutta();
		//write values
		out << wallClock.timePassed << "\t" << wallClock.timeOfDay << "\t" << wallClock.currentIndex;
		for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
			(*geneIt)->writeLogConcentration(out, wallClock.currentIndex, logGamma);
		out << "\n";
	}
	if(verbose){
		gettimeofday(&end, NULL);
		float runtime=(end.tv_sec  - start.tv_sec);
		logFile->write(" done (", runtime, "s)!");
	}
}

bool TGeneVector::runRungeKuttaAndCheck(double maxEpsilon){

    for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
        if((*geneIt)->updateConcentrationAndReturnDiff() > maxEpsilon){
            return false;
        }
    }
    wallClock.increment();

    return true;
}

void TGeneVector::writeRungeKuttaResults(std::string filename, int sparsity){
	logFile->listFlush("Writing Runge-Kutta output for ", wallClock.numTimeSteps, " time points ...");
	//open file
	std::ofstream out;
	out.open(filename.c_str());
	if(!out) throw "Output file '" + filename + "' could not be opened!";

	//write header
	out << "bin";
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
		(*geneIt)->writeLogConcentrationHeader(out);
	out << "\n";

	//write values
	for(long i=0; i<wallClock.numTimeSteps; i=i+sparsity){
		out << i;
		for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
			(*geneIt)->writeLogConcentration(out, i, logGamma);
		}
		out << "\n";
	}
	out.close();
	logFile->write(" done!");
}

//--------------------------------------------------------------
//Function for MCMC
//--------------------------------------------------------------
void TGeneVector::initializeMCMC(TParameters* parameters){
	logFile->startIndent("Initializing MCMC:");

	//check registered parameters
	if(numParamsToUpdate<1) throw "Can not run MCMC: no parameter with defined prior!";
	logFile->startIndent("The following " + toString(paramVec.numParams() + 1) + " parameters will be estimated:");
	paramVec.listParams(logFile);
	logFile->list("scaling factor gamma");
	logFile->endIndent();

	//prepare storage for random numbers
	normalRand = new double[numParamsToUpdate+1];
	unifRand = new double[numParamsToUpdate+1];

	//read real data
	readData(parameters->getParameterString("data"));

	//updating range in units of standard deviations of the prior
	propRange = parameters->getParameterDoubleWithDefault("proposalRange", 0.1);
	logFile->list("Setting proposal ranges to " + toString(propRange) + " units of the standard deviation of the prior");
	if(propRange < 0.00001) throw "Proposal range can not be smaller than 0.00001";
	paramVec.setRange(propRange);

	//scaling factor for normalization of concentrations Gamma
	logGamma = 0.0;
	newLogGamma = 0.0;
	propGamma = parameters->getParameterDoubleWithDefault("proposalGamma", 0.1);
	logFile->list("Setting proposal range for Gamma to " + toString(propGamma));

	MCMCinitialized = true;
	logFile->endIndent();
}

void TGeneVector::writeMCMCHeader(std::ofstream & mcmcOut){
	mcmcOut << "Log(Post.density)\t";
	paramVec.writeHeader(mcmcOut);
	mcmcOut << "\t" << "log(Gamma)" << "\n";
}

void TGeneVector::writeMCMCOutput(std::ofstream & mcmcOut){
	mcmcOut << getCurLogPosteriorDensity() << "\t";
	paramVec.writeCurrentParameters(mcmcOut);
	//mcmcOut << "\t" << logGamma << "\n";
	mcmcOut << "\t" << logGamma << std::endl;
}

void TGeneVector::prepareMCMCRun(){
	if(!MCMCinitialized) throw "Cannot prepare MCMC run: MCMC not initialized!";

	//prepare variables for likelihood calculation
	logFirstNormalDensityTerm = getNumDataPoints() * (log(sigma) + 0.5 * log(2.0 * 3.1415926));
	oldLogPriorDens = paramVec.logPriorDensityTermforHastings();

	//make first Runge Kutta run
	newLogGamma = 0.0;
	getLogLikelihoodTermForHastingsAllDays(newLogGamma);

	//fit good starting Gamma as median scaling across all data and genes
	newLogGamma = getScaleEstimate();
	newLogPosteriorDens = minusOneOverTwoSimgaSquared * getLogLikelihoodTermForHastingsAllDaysOnlyGammaUpdate(newLogGamma) + oldLogPriorDens;
	oldLogPosteriorDens = newLogPosteriorDens;
	logGamma = newLogGamma;

	//set starting variables
	numAccepted = 0;
	numTried = 0;
}

//SZS
void TGeneVector::modifPrepareMCMCRun(){
	if(!MCMCinitialized) throw "Cannot prepare MCMC run: MCMC not initialized!";

	//prepare variables for likelihood calculation
	logFirstNormalDensityTerm = getNumDataPoints() * (log(sigma) + 0.5 * log(2.0 * 3.1415926));
	oldLogPriorDens = paramVec.logPriorDensityTermforHastings();

	//do not make first Runge Kutta run
	newLogGamma = 0.0;

	//fit good starting Gamma as median scaling across all data and genes
	newLogPosteriorDens = -highestPossibleConcentration;
	oldLogPosteriorDens = newLogPosteriorDens;
	logGamma = newLogGamma;

	//set starting variables
	numAccepted = 0;
	numTried = 0;
}
//END_SZS

bool TGeneVector::checkParameters(){
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
		if(!(*geneIt)->checkParameters()) return false;
	}
	return true;
}

void TGeneVector::sampleRandomParameterValuesFromPrior(TRandomGenerator* randomGenerator){
	int numRejected = 0;
	bool paramsSatisfyRules = false;
	while(!paramsSatisfyRules){
		paramVec.sampleFromPriorAll(randomGenerator);
		paramsSatisfyRules = true;
		for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
			paramsSatisfyRules *= (*geneIt)->checkParameters();
		}
		if(!paramsSatisfyRules){
			++numRejected;
			paramVec.resetOldMCMC();
		}
		if(numRejected > 1000) throw "Did not find acceptable parameter combination among 1000 draws from the prior!";
	}
}

void TGeneVector::checkStartingValues(){
	//This function will calculate the posterior density and accept or reject the proposed starting values
	//make Runge Kutta run
	getLogLikelihoodTermForHastingsAllDays(logGamma);

	//SZS
	if (correctConcentrationValues == TRUE)
	{
		//fit good starting Gamma as median scaling across all data and genes
		newLogGamma = getScaleEstimate();

		//calculate posterior density
		newLogPriorDens = paramVec.logPriorDensityTermforHastings();
		newLogPosteriorDens = minusOneOverTwoSimgaSquared * getLogLikelihoodTermForHastingsAllDaysOnlyGammaUpdate(newLogGamma) + newLogPriorDens;
	}
	else
	{
		//Wrong values
		newLogPosteriorDens = -highestPossibleConcentration;
	}
	//END_SZS


	//accept or reject
	if(newLogPosteriorDens < oldLogPosteriorDens){
		//reset
		paramVec.resetOldMCMC();
		//newLogPosteriorDens = oldLogPosteriorDens;
		//newLogPriorDens = oldLogPriorDens;
	} else {
		++numAccepted;
		logGamma = newLogGamma;
		oldLogPosteriorDens = newLogPosteriorDens;
		oldLogPriorDens = newLogPriorDens;
	}
}

double TGeneVector::calcLogLikelihood(double & thisLogGamma){
	//LL is correct up to a constant n*log(1/sqrt(2pi)sigma)
	runRungeKutta(daysDiff[0], true);
	logFile->listFlush("calculating Log-Likelihood ...");
	double LL = minusOneOverTwoSimgaSquared * getLogLikelihoodTermForHastings(thisLogGamma);
	logFile->write(" done!");
	logFile->conclude("LL = " + toString(LL));
	return LL;
}

double TGeneVector::getLogLikelihoodTermForHastings(double & thisLogGamma){
	double ll = 0.0;
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
		ll += (*geneIt)->getLogLikelihoodTermForHastings(thisLogGamma);
	}
	return ll;
}

double TGeneVector::getLogLikelihoodTermForHastingsAllDays(double & thisLogGamma){
	resetRungeKutta();
	if(!multipleDays){
		runRungeKutta(daysDiff[0]);
		//SZS
		if (correctConcentrationValues == FALSE) return highestPossibleConcentration;
		//END_SZS

		return getLogLikelihoodTermForHastings(thisLogGamma);
	} else {
		double LL = 0.0;
		for(int i=0; i<numDays; ++i){
			runRungeKutta(daysDiff[i]);
			LL += getLogLikelihoodTermForHastings(thisLogGamma);
		}
		return LL / numDays;
	}
}

double TGeneVector::getLogLikelihoodTermForHastingsAllDaysOnlyGammaUpdate(double & thisLogGamma){
	//no need to rerun Runge Kutta if there is only one day!
	if(!multipleDays) return getLogLikelihoodTermForHastings(thisLogGamma);
	else {
		resetRungeKutta();
		double LL = 0.0;
		for(int i=0; i<numDays; ++i){
			runRungeKutta(daysDiff[i]);
			LL += getLogLikelihoodTermForHastings(thisLogGamma);
		}
		return LL / numDays;
	}
}

double TGeneVector::getScaleEstimate(){
	//get mean (values are in log, so log should be stable)
	double scale = 0.0;
	int num = 0;
	for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
		if((*geneIt)->hasData()){
			scale += (*geneIt)->getScaleEstimate();
			++num;
		}
	}
	if(num == 0) throw "Can not estimate Gamma: no data provided!";
	return scale / (double) num;
}

void TGeneVector::prepareMCMCStep(TRandomGenerator* randomGenerator){
	//function to store all random numbers to avoid issues when parallelizing
	for(int i=0; i<=numParamsToUpdate; ++i){
		//normalRand[i] = randomGenerator->getStdNormalRandom() * (1.0 - log(heat));
		normalRand[i] = randomGenerator->getStdNormalRandom();
		unifRand[i] = randomGenerator->getRand();

	}
}

void TGeneVector::performMCMCStep(){
	//per iteration, update all parameters once
	bool oneAccepted = false;

	for(int p=0; p<numParamsToUpdate; ++p){
		if(paramVec.updateMCMCOne(normalRand[p])){

			//calculate new LL
			newLogPriorDens = paramVec.logPriorDensityTermforHastings();
			newLogPosteriorDens = minusOneOverTwoSimgaSquared * getLogLikelihoodTermForHastingsAllDays(logGamma) + newLogPriorDens;
			//newLogPosteriorDens = getLogLikelihoodTermForHastingsAllDays(logGamma) + newLogPriorDens;
			hastingsLog = heat*(newLogPosteriorDens - oldLogPosteriorDens);

			if(unifRand[p] < exp(hastingsLog)){
				++numAccepted;
				oldLogPosteriorDens = newLogPosteriorDens;
				oldLogPriorDens = newLogPriorDens;
				//update Gamma if we use only one day, as there is no need to rerun Runge Kutta!
				//but only once per iteration
				if(!oneAccepted && !multipleDays){
					++numTried;
					newLogGamma = logGamma + normalRand[numParamsToUpdate] * propGamma;
					//TODO: check only Gamma!
					newLogPosteriorDens = minusOneOverTwoSimgaSquared * getLogLikelihoodTermForHastingsAllDays(newLogGamma) + oldLogPriorDens;
					hastingsLog = heat*(newLogPosteriorDens - oldLogPosteriorDens);
					if(unifRand[numParamsToUpdate] < exp(hastingsLog)){
						oldLogPosteriorDens = newLogPosteriorDens;
						++numAccepted;
						logGamma = newLogGamma;
					}
				}
				oneAccepted = true;
			} else paramVec.resetOldMCMC();
		}
	}
	numTried += numParamsToUpdate;

	//update Gamma if we have multiple days, as we need to rerun Runge-Kutta!
	if(multipleDays){
		newLogGamma = logGamma + normalRand[numParamsToUpdate] * propGamma;
		newLogPosteriorDens = minusOneOverTwoSimgaSquared * getLogLikelihoodTermForHastingsAllDays(newLogGamma) + oldLogPriorDens;
		hastingsLog = minusOneOverTwoSimgaSquared * (oldLogPosteriorDens - newLogPosteriorDens);
		if(unifRand[numParamsToUpdate] < exp(hastingsLog)){
			++numAccepted;
			logGamma = newLogGamma;
			oldLogPosteriorDens = newLogPosteriorDens;
		}
		++numTried;
	}
}

void TGeneVector::swap(TGeneVector* other){
	//swap heat
	double tmpHeat = heat;
	heat = other->getHeat();
	other->setHeat(tmpHeat);

	//swap acceptance counters
	long tmp = numTried;
	numTried = other->numTried;
	other->numTried = tmp;
	tmp = numAccepted;
	numAccepted = other->numAccepted;
	other->numAccepted = tmp;
}
//--------------------------------------------------------------
//Function to simulate data
//--------------------------------------------------------------
void TGeneVector::initializeSimulations(TParameters* parameters){
	logFile->startIndent("Initializing simulations: ");
	logFile->list("Simulation done with " + toString(wallClock.ftp) + " hours of day and " + toString(24.0 - wallClock.ftp) + " hours of night");
	//register parameters with prior
	logFile->startIndent("Values for the following " + toString(paramVec.numParams()) + " parameters will be drawn from their prior:");
	paramVec.listParams(logFile);
	writeSimulationFiles = true;
	if(parameters->parameterExists("noSimFiles")) writeSimulationFiles = false;
	logFile->endIndent();

	//LR: How many times the program should search for a stable set of parameters
	ParametersToSearch = parameters->getParameterIntWithDefault("testConvergence", 0);
	MaxEpsilon = parameters->getParameterDoubleWithDefault("maxDiff", 0.01 );
	minimalAmplitude = parameters->getParameterDoubleWithDefault("minAmplitude", 1.0 );
	logFile->startIndent("Evaluating a max. of " + toString(ParametersToSearch) + " parameter sets with the restrictions: ");
	logFile->list("minimal Amplitude = " + toString(minimalAmplitude));
	logFile->list("maximal difference in concentration = " + toString(MaxEpsilon));
	logFile->endIndent();
	//END_LR


	//time points at which data is produced
	//if available, read from data file
	if(parameters->parameterExists("data")){
		readData(parameters->getParameterString("data"));
	} else {
		//else, generate data at predefined time points
		std::vector<std::string> times_tmp;
		std::vector<double> times;
		parameters->fillParameterIntoVector("times", times_tmp, ',');
		for(std::vector<std::string>::iterator it=times_tmp.begin(); it!=times_tmp.end(); ++it){
			//check if some are sequences and expand
			std::size_t pos = it->find_first_of('-');
			if(pos != std::string::npos){
				//a sequence of format 20-100:4 (from-to:step)
				std::string original = *it;
				//read from
				double from = stringToDouble(it->substr(0, pos));
				(*it).erase(0, pos+1);
				//read to
				pos = (*it).find_first_of(':');
				if(pos == std::string::npos) throw "Unable to understand format of time points '" + original + "'!";
				double to = stringToDouble(it->substr(0, pos));
				if(to <= from) throw "Unable to understand format of time points '" + original + "', from >= to!";
				//read step
				it->erase(0, pos+1);
				double step = stringToDouble(*it);
				if(step <= 0) throw "Unable to understand format of time points '" + original + "', step <= 0!";
				//no create time points
				for(double i = from; i <= to; i=i+step)
					times.push_back(i);
			} else {
				//a number
				times.push_back(stringToDouble(*it));
			}
		}
		logFile->list("Data will be generated at these times of the day: " + concatenateString(times, ", "));

		int repTime = parameters->getParameterDoubleWithDefault("repTime", 1.0);
		logFile->list("Generating " + toString(repTime) + " data sets per time point");

		for(int i=0; i< repTime; ++i){
			for(std::vector<double>::iterator it = times.begin(); it!=times.end(); ++it){
				for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
					(*geneIt)->addmRNAData(*it, 0.0);
				}
				for(genesWithPreRNAIt=genesWithPreRNA.begin(); genesWithPreRNAIt!=genesWithPreRNA.end(); ++genesWithPreRNAIt){
					(*genesWithPreRNAIt)->addpreRNAData(*it, 0.0);
				}
			}
		}
	}

	writeRungeKutta = parameters->parameterExists("writeFullRungeKutta");
	if(writeRungeKutta && multipleDays) throw "Writing of full Runge-Kutta only possible when running for a single number of days!";

	//gamma: scaling parameter
	logGamma = parameters->getParameterDoubleWithDefault("logGamma", 0.0);
	logFile->list("Using scaling parameter log(Gamma) = " + toString(logGamma));
	logFile->endIndent();
	logFile->endIndent();

}

void TGeneVector::openSimulationOutput(std::string & paramFile){
	logFile->list("Writing parameters used in simulations to '", paramFile, "'");
	simParamOut.open(paramFile.c_str());
	if(!simParamOut) throw "Failed to one file '" + paramFile + "'!";
	simParamOutOpen = true;

	//write header
	simParamOut << "index\tfile\t";
	paramVec.writeHeaderAll(simParamOut);
	simParamOut << "\n";
}

void TGeneVector::simulateData(int index, std::string outname, TRandomGenerator* randomGenerator){
	//chose values from prior
	parameterSetsRejected = 0;

	//sample parameters from prior
	logFile->listFlush("Sampling parameter values from prior ...");
	if(paramVec.numParams() > 0)
		sampleRandomParameterValuesFromPrior(randomGenerator);
	logFile->write(" done!");

	//test multiple parameter sets to find a combination that converges
	if(ParametersToSearch > 0){
		logFile->startNumbering("Testing parameter values for Runge-Kutta convergence:");
		bool paramsOK = false;
		while(!paramsOK){
			logFile->number("Testing parameter combination " + toString(parameterSetsRejected + 1) + ":");
			logFile->addIndent();
			//run Runge Kutta
			logFile->listFlush("Running Runge-Kutta ...");
			resetRungeKutta();
			for(int i=0; i<numDays; ++i){
				runRungeKutta(daysDiff[i]);
			}
			logFile->write(" done!");

			//checking convergence of concentrations
			logFile->listFlush("Checking convergence ...");
			paramsOK = runRungeKuttaAndCheck(MaxEpsilon);
			if(paramsOK){
				//checking for minimal amplitude
				for(geneIt=allGenes.begin(); geneIt!=allGenes.end();++geneIt){
					if((*geneIt)->getAmplitude() < minimalAmplitude){
						paramsOK = false;
						break;
					}
				}
			}
			logFile->write(" done!");

			//fail save for breaking endless loop
			if(!paramsOK){
				logFile->conclude("Parameter combination does not lead to Runge-Kutta convergence.");
				++parameterSetsRejected;
				if (parameterSetsRejected > ParametersToSearch) throw "Did not find a stable parameter set after "+ toString(ParametersToSearch) + " tries!";
				//sample new parameter values
				logFile->conclude("Sampling parameter values from prior.");
				sampleRandomParameterValuesFromPrior(randomGenerator);
			} else {
				logFile->conclude("Parameter combination led to Runge-Kutta convergence.");
				logFile->conclude("Accepting this parameter combination!");
			}
			logFile->endIndent();
		}
	}
	logFile->endIndent();

	//write parameters to parameter file
	std::string filename = outname + "_simulations_" + toString(index) + ".txt";
	if(simParamOutOpen){
		simParamOut << index << "\t" << filename << "\t";
		paramVec.writeCurrentParametersAll(simParamOut);
		simParamOut << "\n";
	}

	//Now simulate data and write output
	if(writeSimulationFiles){
		logFile->startIndent("Simulate data:");
		//open output file

		logFile->list("Will write simulations to file '" + filename + "'.");
		std::ofstream out(filename.c_str());
		if(!out) throw "Failed to one file '" + filename + "'!";

		//Write used params to genes file
		filename = outname + "_simulatedParams_genes_" + toString(index) + ".txt";
		std::ofstream inputfile(filename.c_str());
		inputfile.precision(10);
		//write header
		inputfile << "Gene\tbaseProduction\tmaxProduction\tdegradation\tRNAProcessing\tdelay\thistoricalConcentration" << "\n";
		for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
			(*geneIt)->writeCurValuesToInputFile(inputfile);
		}
		inputfile.close();

		//Write used params to interaction file
		filename = outname + "_simulatedParams_interactions_" + toString(index) + ".txt";
		inputfile.open(filename.c_str());
		inputfile.precision(10);
		//write header
		inputfile << "gene1\tgene2\tstrength" << std::endl;
		for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
			(*geneIt)->writeInteractionsToInputFile(inputfile);
		}
		inputfile.close();

		//run first Runge Kutta day
		logFile->listFlush("Running Runge-Kutta and simulating data ...");
		resetRungeKutta();
		if(!multipleDays && writeRungeKutta){
			std::string RKoutFile = outname + "_fullRungeKutta_" + toString(index) + ".txt";
			runAndWriteRungeKutta(daysDiff[0], RKoutFile);
		} else runRungeKutta(daysDiff[0]);
		for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
			(*geneIt)->simulateData(randomGenerator, sigma, logGamma);
		}

		//run multiple days?
		if(multipleDays){
			for(int i=0; i<numDays; ++i){
				runRungeKutta(daysDiff[i]);
				for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
					(*geneIt)->simulateDataAdd(randomGenerator, sigma, logGamma);
			}
		}
		logFile->write(" done!");

		//write simulations
		logFile->listFlush("Writing simulation to  file ...");
		out << "Gene\tTime\tConcType\tValueRNA" << "\n";
		double nd = numDays;
		for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt){
			(*geneIt)->writeObservedData(out, nd);
		}
		out.close();
		logFile->write(" done!");
	}

	logFile->endIndent();
}
