/*
 * TGen.h
 *
 *  Created on: Apr 22, 2014
 *      Author: wegmannd
 */

#ifndef TGEN_H_
#define TGEN_H_

#include "TConcentrations.h"
#include "TLog.h"
#include "TParameters.h"
#include "stringFunctions.h"
#include "TParam.h"
#include <math.h>
#include <algorithm>
#include <time.h>
#include <omp.h>

//SZS
//modified due Win platform compatibility
#if defined _WIN32
	// Windows stuff
	#pragma warning(disable: 4244)
	#include "linux_time.h"
#else
	// Other stuff
	#include <sys/time.h>	
#endif
//END_SZS

//--------------------------------------------------------------------------------
class TmRNA;
class TBaseInteraction{
public:
	TParam c;
	std::string type;

	TBaseInteraction(){type="base";};
	virtual ~TBaseInteraction(){};
	virtual TBaseInteraction* createCopy(std::vector<TmRNA*> & genes, TClock* Clock){throw "Creating copies not implemented for TBaseInteraction!";};
	virtual void registerParameters(TParamVector* paramVec);
	virtual double getValueNow(){throw "TBaseInteraction can not return a value!";};
	virtual double getValueNowPlusHalfDeltaT(){throw "TBaseInteraction can not return a value!";};
	virtual double getValueNowPlusDeltaT(){throw "TBaseInteraction can not return a value!";};
	virtual std::string getString(){return "I'm the TBaseInteraction class!";};
	virtual void writeCurValuesToInputFile(std::ofstream & out){throw "TBaseInteraction can not write itself!";};
};


class TGeneInteraction:public TBaseInteraction{
public:
	TmRNA* interactingGene;
	TConcentrationsWithDelay* interactingConcentrations;

	TGeneInteraction(TmRNA* InteractingGene);
	TGeneInteraction(std::string geneName, TmRNA* InteractingGene, std::string & paramC);
	~TGeneInteraction(){};
	TBaseInteraction* createCopy(std::vector<TmRNA*> & genes, TClock* Clock);
	double getValueNow();
	double getValueNowPlusHalfDeltaT();
	double getValueNowPlusDeltaT();
	std::string getString();
	void writeCurValuesToInputFile(std::ofstream & out);
};

class TLight:public TBaseInteraction{
public:
	TClock* myClock;

	TLight(std::string geneName, TClock* Clock, std::string & paramC);
	TLight(TClock* Clock);
	TBaseInteraction* createCopy(std::vector<TmRNA*> & genes, TClock* Clock);
	double getValue(double & timeOfDay);
	double getValueNow();
	double getValueNowPlusHalfDeltaT();
	double getValueNowPlusDeltaT();
	std::string getString();
	void writeCurValuesToInputFile(std::ofstream & out);
};
//--------------------------------------------------------------------------------
class TmRNA{
public:
	std::string name;
	TClock* myClock;
	TParamLog baseProduction;
	TParamLog maxProduction;
	TParamLog degradation;
	char type;

	TParam delay; //in hours
	int delayInDeltaT;
	bool hasInteractions;
	std::vector<TBaseInteraction*> interactions;
	std::vector<TBaseInteraction*>::iterator interactionIt;
	//numerical solution of DDE
	TConcentrationsWithDelay* mRNAConcentrations;
	double historicalConcentration;
	bool mRNAConcentrationsInitialized;
	double k1, k2, k3, k4;
	double oneSixth, oneThird;

	TmRNA(std::string Name, std::string BaselineProduction, std::string MaxProduction, std::string Degradation, std::string Delay, double HistoricalConcentration, TClock* Clock);
	TmRNA(TmRNA* other, TClock* Clock);
	virtual ~TmRNA(){
		for(interactionIt=interactions.begin(); interactionIt!=interactions.end(); ++interactionIt)
			delete (*interactionIt);
		if(mRNAConcentrationsInitialized) delete mRNAConcentrations;
	};
	void initRKParams();
	void addInteraction(TmRNA* InteractingGene, std::vector<std::string> & line);
	void copyInteractions(TmRNA* other, std::vector<TmRNA*> & genes);
	virtual bool checkParameters();

	void addmRNAData(double time, double value);
	virtual bool hasData();
	virtual void registerParameters(TParamVector* paramVec);
	virtual void initializeRungeKutta();
	double gompertz(double & A, double & minusB, double & minusValue);
	double gompertzAtZero(double & A, double & minusB);
	virtual void updateConcentration();
	virtual double updateConcentrationAndReturnDiff();
	virtual double getAmplitude();
	double getDelayedConcentration();
	double getDelayedPlusHalfDeltaTConcentration();
	double getDelayedPlusDeltaTConcentration();
	double getConcentration(int bin);
	virtual double getLogLikelihoodTermForHastings(double & logGamma);
	virtual double getScaleEstimate();
	virtual void print(TLog* logfile);
	std::string getDelayedConcentrationString();
	virtual void simulateData(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma);
	virtual void simulateDataAdd(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma);
	virtual void writeObservedData(std::ofstream & out, double & numDays);
	virtual void writeCurValuesToInputFile(std::ofstream & out);
	void writeInteractionsToInputFile(std::ofstream & out);
	virtual void writeLogConcentrationHeader(std::ofstream & out);
	virtual void writeLogConcentration(std::ofstream & out, long & bin, double & logGamma);

	//SZS
	double getmRNAConcentration();
	virtual double getPreRNAConcentration();
	//END_SZS

};

class TmRNAWithPreRNA:public TmRNA{
public:
	TConcentrations* preRNAConcentrations;
	bool preRNAConcentrationsInitialized;
	TParamLog RNAProcessing;
	double k1mRNA, k2mRNA, k3mRNA, k4mRNA;


	TmRNAWithPreRNA(std::string Name, std::string BaselineProduction, std::string MaxProduction, std::string Degradation, std::string RNAProcessing, std::string Delay, double HistoricalConcentration, TClock* Clock);
	TmRNAWithPreRNA(TmRNAWithPreRNA* other, TClock* Clock);
	~TmRNAWithPreRNA(){
		if(preRNAConcentrationsInitialized) delete preRNAConcentrations;
	};
	bool checkParameters();
	void initRKParamsWithpreRNA();
	void addpreRNAData(double time, double value);
	bool hasData();
	void registerParameters(TParamVector* paramVec);
	void initializeRungeKutta();
	void updateConcentration();
	double updateConcentrationAndReturnDiff();
	double getAmplitude();
	double getLogLikelihoodTermForHastings(double & logGamma);
	double getScaleEstimate();
	void print(TLog* logfile);
	void simulateData(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma);
	void simulateDataAdd(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma);
	void writeObservedData(std::ofstream & out, double & numDays);
	void writeCurValuesToInputFile(std::ofstream & out);
	void writeLogConcentrationHeader(std::ofstream & out);
	void writeLogConcentration(std::ofstream & out, long & bin, double & logGamma);

	//SZS
	double getPreRNAConcentration();
	//END_SZS

};
//--------------------------------------------------------------------------------


#endif /* TGEN_H_ */
