#ifndef TRandomGenerator_H_
#define TRandomGenerator_H_

#include <cmath>
#include <iostream>
//#include <time.h>

//SZS
//modified due Win platform compatibility
#if defined _WIN32
	// Windows stuff
	#pragma warning(disable: 4244)
	#include "linux_time.h"
	#define M_E 2.71828182845904523536
#else
	// Other stuff
	#include <sys/time.h>	
#endif
//END_SZS


class TRandomGenerator{
public:
	long usedSeed;

	TRandomGenerator(long addToSeed){
		init(addToSeed);
	};
	TRandomGenerator(long addToSeed, bool seedIsFixed){
		if(!seedIsFixed) init(addToSeed);
		else {
			if(addToSeed<0) addToSeed=-addToSeed;
	        usedSeed=addToSeed;
	        _Idum=-addToSeed;
		}
	};
	TRandomGenerator(){
			init(0);
	};
	~TRandomGenerator(){};
	void init(long addToSeed);
	double getRand(){ return ran3(); };
	double getRand(double min, double max);
	int getRand(int min, int maxPlusOne);
	long getRand(long min, long maxPlusOne);
	double getStdNormalRandom();
	double getNormalRandom(double dMean, double dStdDev);
	long get_randomSeedFromCurrentTime(long & addToSeed);
	double getBiomialRand(double pp, int n);
	double gammaln(float xx);
	double binomCoeffLn(int n, int k);
	double getGammaRand(double a, double b);
	double getGammaRand(double a);
	double getGammaRand(int ia);
	double getBetaRandom (double alpha, double beta, double a, double b);
	double getBetaRandom (double alpha, double beta);
	double exponentialRand(double lambda);
	double getPoissonRandom(double lambda);
	int getGeometricRandom(double & p);

private:
	long _Idum;
	double ran3();
};

#endif /* TRandomGenerator_H_ */
