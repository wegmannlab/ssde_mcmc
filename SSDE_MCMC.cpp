/*
 * ApproxWF.cpp
 *
 *  Created on: Feb 12, 2014
 *      Author: wegmannd
 */


//SZS
//Modified include file list
//due Win platform compatibility
#include "SSDE_MCMC.h"
#include "stringFunctions.h"
#include "TParameters.h"
#include "TSSDE_MCMC.h"
//END_SZS


//SZS
//Modified main file name
int old_main(int argc, char* argv[]){

	//modified due Win platform compatibility
	#if defined _WIN32
	  // Windows stuff
		InitTimeMeasFrequency();
		struct timeval2 start, end;	
	#else
	  // Other stuff
		struct timeval start, end;
	#endif
	gettimeofday(&start, NULL);
//END_SZS

	//log file
  TLog logfile;
  
	logfile.newLine();
	logfile.write(" SSDE_MCMC ");
	logfile.write("***********");
	try{
		//read parameters from the command line
		TParameters myParameters(argc, argv, &logfile);

		//verbose?
		bool verbose=myParameters.parameterExists("verbose");
		if(!verbose) logfile.list("Running in silent mode (use 'verbose' to get a status report on screen)");
		logfile.setVerbose(verbose);

		//open log file that handles the output
		std::string logFilename=myParameters.getParameterStringWithDefault("logFile", "SSDE_MCMC.log");
		if(logFilename.length()>0){
			logfile.openFile(logFilename.c_str());
			logfile.writeFileOnly(" SSDE_MCMC ");
			logfile.writeFileOnly("***********");
		}
		if(myParameters.hasInputFileBeenRead())
			logfile.list("parameters read from inputfile '" + myParameters.getInputFile() + "'");

		//what to do?
		TSSDE_MCMC ssde_mcmc(&logfile, &myParameters);
		std::string task=myParameters.getParameterString("task");
		if(task=="MCMC") ssde_mcmc.performMCMCInference();
		else if(task=="simulate") ssde_mcmc.simulate();
		else if(task=="LL") ssde_mcmc.calcLikelihood();
		else throw "Unknown task '" + task + "'!";

		//end of program
		//write unsused parameters
		std::string unusedParams=myParameters.getListOfUnusedParameters();
		if(unusedParams!="") logfile.warning("The following parameters were not used: " + unusedParams + "!");
	 }
	 catch (const std::string & error){
		 logfile.error(error);
		 return 1;
	 }
	 catch (const char* error){
		 logfile.error(error);
		 return 1;
	 }
	 catch (...){
		 std::cerr << "\n\n" << "ERROR: unhandled error!" << std::endl;
		 return 1;
	 }

	 gettimeofday(&end, NULL);
	 float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	 logfile.list("program terminated in ", runtime, " min!");
	 logfile.close();
	
	 return 0;
}


