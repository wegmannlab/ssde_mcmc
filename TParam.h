/*
 * TParam.h
 *
 *  Created on: Jun 4, 2014
 *      Author: wegmannd
 */

#ifndef TPARAM_H_
#define TPARAM_H_

#include "stringFunctions.h"
#include "TRandomGenerator.h"
#include "TLog.h"
#include <iostream>

//-------------------------------------------------------------
//TParam
//-------------------------------------------------------------
class TParam{
protected:
	double sigma, oldVal, range, min, max;
	bool hasMinMax;

//data access
public: 
	double mean;
	double twoSigmaSquared;

public:
	std::string name;
	bool hasPrior;
	bool rangeSet;
	double curVal;

	TParam(){
		mean = -1.0;
		sigma = -1.0;
		twoSigmaSquared = -1.0;
		hasPrior = false;
		rangeSet = false;
		curVal = -1.0;
		oldVal = -1.0;
		range = -1.0;
		hasMinMax = false;
		min = -1.0;
		max = -1.0;
	}

	TParam(std::string Name, std::string & val){
		init(Name, val);
	};

	virtual ~TParam(){};

	virtual void init(std::string Name, std::string & val){
		name = Name;
		std::size_t pos = val.find_first_of(",");
		if(pos==std::string::npos){
			mean = stringToDoubleCheck(val);
			sigma = -1;
			hasPrior = false;
		} else {
			mean = stringToDoubleCheck(val.substr(0, pos));
			val.erase(0,pos+1);

			//check if min/max is given
			pos = val.find_first_of("[");
			if(pos==std::string::npos){
				//check if range is given
				pos = val.find_first_of(",");
				if(pos==std::string::npos){
					//not give -> whole string is sigma
					sigma = stringToDoubleCheck(val);
				} else {
					//given
					sigma = stringToDoubleCheck(val.substr(0, pos));
					val.erase(0,pos+1);
					setRange(stringToDoubleCheck(val));
				}
			} else {
				//min max is given
				sigma = stringToDoubleCheck(val.substr(0, pos));
				val.erase(0,pos+1);
				pos = val.find_first_of(",");
				if(pos==std::string::npos) throw "Unable to understand range for prior on '" + Name + "'!";
				double readMin = stringToDoubleCheck(val.substr(0, pos));
				val.erase(0,pos+1);
				pos = val.find_first_of("]");
				if(pos==std::string::npos) throw "Unable to understand range for prior on '" + Name + "'!";
				double readMax = stringToDoubleCheck(val.substr(0, pos));
				setMinMax(readMin, readMax);

				//check if range is given at end
				pos = val.find_first_of(",");
				if(pos != std::string::npos){
					//delete everything before and including ,
					val.erase(0,pos+1);
					setRange(stringToDoubleCheck(val));
				}
			}
			twoSigmaSquared = 2.0 * sigma * sigma;
			hasPrior = true;
		}
		curVal = mean;
		oldVal = mean;
	};

	virtual void init(TParam* other){
		name = other->name;
		mean = other->mean;
		sigma = other->sigma;
		twoSigmaSquared = other->twoSigmaSquared;
		hasPrior = other->hasPrior;
		curVal = other->curVal;
		oldVal = other->oldVal;
		hasMinMax = other->hasMinMax;
		min = other->min;
		max = other->max;
		range = other->range;
	};

	virtual void init(std::string Name, std::string & val, double Min, double Max){
		init(Name, val);
		setMinMax(Min, Max);
	};

	void setRange(double Range){
		if(!rangeSet){
			range = Range * sigma;
			if(hasMinMax && range > (max-min)/4.0) range = (max-min)/4.0;
			rangeSet = true;
		}
	};

	virtual void setMinMax(double Min, double Max){
		min = Min;
		if(min<0) throw "Parameter '" + name + "' can not have a negative minimum!";
		max = Max;
		if(min>=max) throw "Minimum > maximum for parameter '" + name + "'!";
		hasMinMax = true;

		if(hasMinMax && range > (max-min)/4.0) range = (max-min)/4.0;
	}

	virtual std::string getString(){
		if(hasPrior){
			return name + "~N(" + toString(mean) + "," + toString(sigma) + ")";
		} else {
			return toString(mean);
		}
	};

	virtual void sampleFromPrior(TRandomGenerator* randomGenerator){
		if(hasPrior){
			oldVal = curVal;
			curVal = max + 1.0;
			if(hasMinMax){
				while(curVal > max || curVal < min)
					curVal = randomGenerator->getNormalRandom(mean, sigma);
			} else curVal = randomGenerator->getNormalRandom(mean, sigma);
		}
	};

	virtual void proposeMCMC(double & normalRand){
		if(hasPrior){
			oldVal = curVal;
			curVal += normalRand * range;

			if(hasMinMax){
				//mirror at min and max
				while(curVal > max || curVal < min){
					if(curVal > max) curVal = 2.0 * max - curVal;
					if(curVal < min) curVal = 2.0 * min - curVal;
				}
			}
		}
	};

	virtual void resetOldMCMC(){
		if(hasPrior){
			curVal = oldVal;
		}
	};

	virtual double& currentValue(){
		return curVal;
	};

	double logPriorDensityTermforHastings(){
		double tmp = curVal - mean;
		return - (tmp * tmp  / twoSigmaSquared);
	};
};

//-------------------------------------------------------------
//TParamLog
//-------------------------------------------------------------
class TParamLog:public TParam{
protected:
	double curValNatural;

public:
	TParamLog():TParam(){
		curValNatural = -1;
	};

	TParamLog(std::string Name, std::string & val){
		init(Name, val);
	};

	virtual void init(std::string Name, std::string & val){
		TParam::init(Name, val);
		curValNatural = exp(curVal);
	};

	void init(TParamLog* other){
		TParam::init(other);
		curValNatural = exp(curVal);
	}

	void setMinMax(double Min, double Max){
		min = Min;
		max = Max;
		if(min>=max) throw "Minimum > maximum for parameter '" + name + "'!";
		hasMinMax = true;
	}

	void setMinMaxExtremes(double Min, double Max){
		if(hasMinMax){
			if(Min > min) min = Min;
			if(Max < max) max = Max;
		} else {
			min = Min;
			max = Max;
			hasMinMax = true;
		}
		if(min>=max) throw "Minimum > maximum for parameter '" + name + "'!";
		hasMinMax = true;
	}

	std::string getString(){
		if(hasPrior){
			if(hasMinMax) return "exp( " + name + "~N(" + toString(mean) + "," + toString(sigma) + ")|" + toString(min) + "," + toString(max) + "|";
			else return "exp( " + name + "~N(" + toString(mean) + "," + toString(sigma) + ") )";
		} else {
			return toString(exp(mean));
		}
	};

	virtual void sampleFromPrior(TRandomGenerator* randomGenerator){
		TParam::sampleFromPrior(randomGenerator);
		curValNatural = exp(curVal);
	};

	void proposeMCMC(double & normalRand){
		TParam::proposeMCMC(normalRand);
		curValNatural = exp(curVal);
	};

	void resetOldMCMC(){
		TParam::resetOldMCMC();
		curValNatural = exp(curVal);
	}

	virtual double& currentValue(){
		return curValNatural;
	}
};

//-------------------------------------------------------------
//TParamRule
//-------------------------------------------------------------
//first > second
class TParamRule{
public:
	TParam* first;
	TParam* second;

	TParamRule(TParam* First, TParam* Second){
		first = First;
		second = Second;
	};

	bool check(){
		if(first->curVal > second->curVal) return true;
		return false;
	};
};

//-------------------------------------------------------------
//TParamVector
//-------------------------------------------------------------
class TParamVector{
private:
	std::vector<TParam*> paramVec;
	std::vector<TParam*> paramVecAll;
	std::vector<TParam*>::iterator it;
	std::vector<TParam*>::iterator mcmcIt;
	std::vector<TParamRule*> rules;
	std::vector<TParamRule*>::iterator ruleIt;
	bool updateingOneByOne;

public:
	TParamVector(){
		updateingOneByOne = false;
	};

	~TParamVector(){
		for(ruleIt = rules.begin(); ruleIt != rules.end(); ++ruleIt)
			delete *ruleIt;
	};

	void setRange(double Range){
		for(it=paramVec.begin(); it!=paramVec.end(); ++it){
			(*it)->setRange(Range);
		}
	};

	void addParam(TParam* p){
		if(p->hasPrior)
			paramVec.push_back(p);
		paramVecAll.push_back(p);
	};

	void addRuleFirstLargerSecond(TParam* first, TParam* second){
		rules.push_back(new TParamRule(first, second));
	};

	void sampleFromPriorAll(TRandomGenerator* randomGenerator){
		bool ok = false;
		while(!ok){
			for(it=paramVec.begin(); it!=paramVec.end(); ++it){
				(*it)->sampleFromPrior(randomGenerator);
			}
			if(checkRules()) ok = true;
			else {
				resetOldMCMC();
				std::cout << "RESET!" << std::endl;
			}
		}
		updateingOneByOne = false;
	}

	void updateMcmcAll(TRandomGenerator* randomGenerator){
		bool ok = false;
		while(!ok){
			for(it=paramVec.begin(); it!=paramVec.end(); ++it){
				double rand = randomGenerator->getNormalRandom(0.0, 1.0);
				(*it)->proposeMCMC(rand);
			}
			if(checkRules()) ok = true;
			else resetOldMCMC();
		}
		updateingOneByOne = false;
	};

	bool updateMCMCOne(double & normalRand){
		if(!updateingOneByOne) mcmcIt=paramVec.begin();
		else {
			++mcmcIt;
			if(mcmcIt==paramVec.end()) mcmcIt=paramVec.begin();
		}
		updateingOneByOne = true;

		//update and check if update is accepted
		(*mcmcIt)->proposeMCMC(normalRand);
		if(checkRules()) return true;
		else resetOldMCMC();
		return false;
	};

	void resetOldMCMC(){
		if(updateingOneByOne) (*mcmcIt)->resetOldMCMC();
		else {
			for(it=paramVec.begin(); it!=paramVec.end(); ++it){
				(*it)->resetOldMCMC();
			}
		}
	};

	bool checkRules(){
		for(ruleIt = rules.begin(); ruleIt != rules.end(); ++ruleIt){
			if(!(*ruleIt)->check()) return false;
		}
		return true;
	};

	void writeHeader(std::ostream & out){
		it=paramVec.begin();
		out << (*it)->name; ++it;
		for(; it!=paramVec.end(); ++it){
			out << "\t" << (*it)->name;
		}
	};
	void writeCurrentParameters(std::ostream & out){
		it=paramVec.begin();
		out << (*it)->curVal; ++it;
		for(; it!=paramVec.end(); ++it){
			out << "\t" << (*it)->curVal;
		}
	};
	void writeHeaderAll(std::ostream & out){
		it=paramVecAll.begin();
		out << (*it)->name; ++it;
		for(; it!=paramVecAll.end(); ++it){
			out << "\t" << (*it)->name;
		}
	};
	void writeCurrentParametersAll(std::ostream & out){
		it=paramVecAll.begin();
		out << (*it)->curVal; ++it;
		for(; it!=paramVecAll.end(); ++it){
			out << "\t" << (*it)->curVal;
		}
	};
	int numParams(){
		return paramVec.size();
	};
	void listParams(TLog* logfile){
		for(it=paramVec.begin(); it!=paramVec.end(); ++it){
			logfile->list((*it)->name);
		}
	};
	double logPriorDensityTermforHastings(){
		double pp = 0.0;
		for(it=paramVec.begin(); it!=paramVec.end(); ++it){
			pp += (*it)->logPriorDensityTermforHastings();
		}
		return pp;
	};

	std::string getCurrentParamName(){
		if(!updateingOneByOne) mcmcIt=paramVec.begin();
		if(mcmcIt==paramVec.end()) return "";
		else return (*mcmcIt)->name;
	};
};



#endif /* TPARAM_H_ */
