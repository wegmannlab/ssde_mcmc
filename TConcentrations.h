/*
 * TConcentrations.h
 *
 *  Created on: May 19, 2015
 *      Author: wegmannd
 */

//SZS
//Modified iclude file list
#include "stringFunctions.h"
//END_SZS

#ifndef TCONCENTRATIONS_H_
#define TCONCENTRATIONS_H_

#include <vector>
#include "TRandomGenerator.h"
#include <fstream>
#include  <utility>

class TConcentrationsWithDelay;
class TClock{
public:
	double timeOfDay, timeOfDayPlusDeltaT, timeOfDayPlusHalfDeltaT;
	double timePassed;
	double hoursPerDay;
	double deltaT;
	long numTimeSteps;
	long currentIndex;
	long nextIndex;
	int day;
	double ftp; //time at which there is light in hours
	std::vector<TConcentrationsWithDelay*> concentrationObjects;
	std::vector<TConcentrationsWithDelay*>::iterator conObjIt;

	TClock();
	~TClock(){};
	void setStepsPerDay(long & stepsPerDay);
	void setFtp(double Ftp);
	void reset();
	void increment();
	void addConcentrationObject(TConcentrationsWithDelay* ConcentrationObject);
	long getIndexFromTime(double & time);
	bool isLight();
	bool isLight(double & time);
};

class TConcentrations{
public:
	double* concentrations;
	TClock* myClock;
	std::vector< std::pair<long, double> > observedData;
	std::vector< std::pair<long, double> >::iterator dataIt;
	bool hasData;

	TConcentrations(TClock* Clock, const double & HistoricalConcentration);
	TConcentrations(TConcentrations* other, TClock* Clock); // copy constructor
	virtual ~TConcentrations(){delete[] concentrations;};
	virtual void reset(const double & HistoricalConcentration);
	void addData(double & time, double & value);
	double at(int bin){	return concentrations[bin];	}; //Note: does not check validity!
	double current(){ return concentrations[myClock->currentIndex];};
	double currentPlusHalf(){return (concentrations[myClock->currentIndex] + concentrations[myClock->nextIndex])/2;};
	double next(){return concentrations[myClock->nextIndex];};
	void setNext(double val){ concentrations[myClock->nextIndex] = val; };
	double atLogScaled(int bin, double & logGamma){	return log(concentrations[bin]) + logGamma;	}; //Note: does not check validity!
	int size(){ return myClock->numTimeSteps;};
	double getLogLikelihoodTermForHastings(double & logGamma);
	double getScaleEstimate();
	void simulateData(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma);
	void simulateDataAdd(TRandomGenerator* RandomGenerator, double & sigma, double & logGamma);
	virtual void writeObservedData(std::ofstream & out, std::string & geneName, double & numDays, std::string type);
	double getAmplitude();
};

class TConcentrationsWithDelay:public TConcentrations{
public:
	long delayIndex;
	long delayPlusOneIndex;
	double delayedPlusHalfConcentration;

	TConcentrationsWithDelay(TClock* Clock, const int & delayInDeltaT, const double & HistoricalConcentration);
	TConcentrationsWithDelay(TConcentrationsWithDelay* other, TClock* Clock); // copy constructor
	~TConcentrationsWithDelay(){};
	void reset(const int & delayInDeltaT, const double & HistoricalConcentration);
	void increment();
	double delay(){ return concentrations[delayIndex];};
	double delayPlusHalf(){ return delayedPlusHalfConcentration;};
	double delayPlusOne(){ return concentrations[delayPlusOneIndex];};
};


#endif /* TCONCENTRATIONS_H_ */
