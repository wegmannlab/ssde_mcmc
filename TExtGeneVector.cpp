//SZS
//2015.11.11

#include "TExtGeneVector.h"

//--------------------------------------------------------------------------------
//TExtGeneVector
//--------------------------------------------------------------------------------
TExtGeneVector::TExtGeneVector(TLog* LogFile, TParameters* parameters) : TGeneVector(LogFile, parameters)
{
	//Set input parameters with default values
	executionMode = 0;									//classic execution
	expInterpolation = 0;								//no exp interpolation
	rungeKuttaDaysTruncated = 0;				//all days are executed
	coarseFineResolutionRatio = 1;	  	//coarse = fine resolution

	//Initialize helper parameters
	isFirstTimeExecuted = TRUE;
	speedRoutineAllowed = TRUE;

	//Debug parameters
	debugPropThreshold = 1.000000000001;

	//Set functional parameters
	lastAcceptedCritValue = highestPossibleConcentration;
	TotalCases = 0;
	RejectedCases = 0;
}

TExtGeneVector::TExtGeneVector(TExtGeneVector* other) : TGeneVector(other)
{
	//Set input parameters with default values
	executionMode = other->executionMode;														//chain execution
	expInterpolation = other->expInterpolation;											//chain interpolation
	rungeKuttaDaysTruncated = other->rungeKuttaDaysTruncated;				//chain days truncation 
	coarseFineResolutionRatio = other->coarseFineResolutionRatio;	  //chain coarse - fine resolution

	//Initialize helper parameters
	isFirstTimeExecuted = TRUE;
	speedRoutineAllowed = TRUE;

	//Debug parameters
	debugPropThreshold = 1.000000000001;

	//Set functional parameters
	lastAcceptedCritValue = highestPossibleConcentration;
	TotalCases = 0;
	RejectedCases = 0;
}

//Initialize, if exists, the new additional parameters from the command line 
void TExtGeneVector::initializeAdditionalParameters(TParameters* parameters)
{
	executionMode = parameters->getParameterIntWithDefault("ExecMode", executionMode);
	expInterpolation = parameters->getParameterIntWithDefault("ExpInterpolation", expInterpolation);
	rungeKuttaDaysTruncated = parameters->getParameterIntWithDefault("RKDaysTruncated", rungeKuttaDaysTruncated);
	coarseFineResolutionRatio = parameters->getParameterIntWithDefault("CoarseFineRatio", coarseFineResolutionRatio);
}

bool TExtGeneVector::initializeAdditionalFields()
{
	unsigned int i;
	int j,k;
	double histConc;
	int startIdx1, startIdx2;

	//Initialize some calculation helper variables
	oneSixth = 1.0/6.0;
	oneThird = 2.0*oneSixth;
	twoThird = 2.0*oneThird;

	//Prepare the global conentrations array
	nrOfAllGenes = allGenes.size();
	nrOfGenes = genes.size();
	nrOfGenesWithPreRNA = genesWithPreRNA.size();

	numOfConcArrays = nrOfGenes + 2*nrOfGenesWithPreRNA;

	//Set the light limit
	deltaT = wallClock.deltaT;
	halfDeltaT = deltaT * 0.5;

	lightLimitInDeltaT = allGenes[0]->myClock->ftp/deltaT;

	//Get the maximal size of experiment in days
	maxExperimentSizeInDays = 0;
	maxStepsInOneDay = wallClock.numTimeSteps;
	for (i=0;i<days.size();++i)
		if (maxExperimentSizeInDays < days[i]) maxExperimentSizeInDays = days[i];
	sizeOfConcArray = (maxExperimentSizeInDays + 1) * maxStepsInOneDay;
	twiceSizeOfConcArray = 2 * sizeOfConcArray;

	//
	//Memory allocation of several helper arrays
	//

	//Allocate the array of gene types
	geneType = (int*) (new (std::nothrow) int [nrOfAllGenes]);
	if (geneType == NULL)
	{
		freeArrays();
		return false;
	}
	for (j=0;j<nrOfGenes;++j) geneType[j] = 1;		//gene with only mRNA
	for (j=0;j<(nrOfAllGenes - nrOfGenes);++j) geneType[nrOfGenes + j] = 2;		//gene with preRNA and mRNA

	//Allocate the array of concentrations
	//and fill the static part with historical values
	concArray = new (std::nothrow) double [numOfConcArrays * sizeOfConcArray + 1];
	if (concArray == NULL)
	{
		freeArrays();
		return false;
	}

	for (j=0;j<nrOfGenes;++j)
	{
		startIdx1 = j*sizeOfConcArray;
		histConc = genes[j]->historicalConcentration;
		for (k=0;k<=maxStepsInOneDay;++k)
		{
			concArray[startIdx1+k] = histConc;
		}
	}

	for (j=0;j<nrOfGenesWithPreRNA;++j)
	{
		startIdx1 = (2*j+nrOfGenes)*sizeOfConcArray;
		startIdx2 = (2*j+nrOfGenes+1)*sizeOfConcArray;
		histConc = genesWithPreRNA[j]->historicalConcentration;
		for (k=0;k<=maxStepsInOneDay;++k)
		{
			concArray[startIdx1+k] = histConc;
			concArray[startIdx2+k] = histConc;
		}
	}

	//Allocate the array of delays for each gene
	delayArrayInDeltaT = new (std::nothrow) unsigned int [nrOfAllGenes];
	if (delayArrayInDeltaT == NULL)
	{
		freeArrays();
		return false;
	}

	//Allocate the array of minimal delays for interactions of each gene
	minimumDelayArrayInDeltaT = new (std::nothrow) unsigned int [nrOfAllGenes];
	if (minimumDelayArrayInDeltaT == NULL)
	{
		freeArrays();
		return false;
	}

	//Allocate the array of degradation for each gene
	currentDegradation = new (std::nothrow) double [nrOfAllGenes];
	if (currentDegradation == NULL)
	{
		freeArrays();
		return false;
	}

	//Allocate the array of term A for each gene
	value_A = new (std::nothrow) double [nrOfAllGenes];
	if (value_A == NULL)
	{
		freeArrays();
		return false;
	}

	//Allocate the array of term minusB for each gene
	value_minusB = new (std::nothrow) double [nrOfAllGenes];
	if (value_minusB == NULL)
	{
		freeArrays();
		return false;
	}

	//Allocate the array of term minusSumC for each gene
	value_minusSumC = new (std::nothrow) double [nrOfAllGenes];
	if (value_minusSumC == NULL)
	{
		freeArrays();
		return false;
	}

	//Array of last minusSumC values
	lastSumC = (double*) (new (std::nothrow) double [nrOfAllGenes]);
	if (lastSumC == NULL)
	{
		freeArrays();
		return false;
	}

	//Array of last exp(minusSumC) values
	lastExpSumC = (double*) (new (std::nothrow) double [nrOfAllGenes]);
	if (lastExpSumC == NULL)
	{
		freeArrays();
		return false;
	}

	//Allocate the array of term RNAProcessing for only genes with PreRNA
	value_RNAProcessing = (double*) (new (std::nothrow) double [nrOfGenesWithPreRNA]);
	if (value_RNAProcessing == NULL)
	{
		freeArrays();
		return false;
	}

	//Allocate the array of various slopes
	constSlope = (double*) (new (std::nothrow) double [nrOfAllGenes]);
	if (constSlope == NULL)
	{
		freeArrays();
		return false;
	}

	darkConstSlope = (double*) (new (std::nothrow) double [nrOfAllGenes]);
	if (darkConstSlope == NULL)
	{
		freeArrays();
		return false;
	}

	lastSlope = (double*) (new (std::nothrow) double [nrOfAllGenes]);
	if (lastSlope == NULL)
	{
		freeArrays();
		return false;
	}

	//Allocate and fill interactions related arrays 
	//Ptr to genes, numer of interactions and interactions array
	genePtr = (void**) (new (std::nothrow) void* [nrOfAllGenes]);
	if (genePtr == NULL)
	{
		freeArrays();
		return false;
	}

	interactionsCounter = new (std::nothrow) int [nrOfAllGenes];
	if (interactionsCounter == NULL)
	{
		freeArrays();
		return false;
	}

	interactionsArray = new (std::nothrow) int [nrOfAllGenes*nrOfAllGenes];
	if (interactionsArray == NULL)
	{
		freeArrays();
		return false;
	}
	for (i=0;i<((unsigned int) nrOfAllGenes*nrOfAllGenes);++i) interactionsArray[i] = -1;

	k = 0;
	for (j=0;j<nrOfGenes;++j)
	{
		genePtr[j] = (void*) genes[j];
		interactionsCounter[j] = genes[j]->interactions.size();
		k++;
	}

	for (j=0;j<nrOfGenesWithPreRNA;++j)
	{
		genePtr[j+k] = (void*) genesWithPreRNA[j];
		interactionsCounter[j+k] = genesWithPreRNA[j]->interactions.size();
	}

	void* ptr;
	for (i=0;i<((unsigned int) nrOfGenes);++i)
	{
		for (j=0;j<interactionsCounter[i];++j)
		{
			k=0;
			ptr = ((TGeneInteraction*) genes[i]->interactions[j])->interactingGene;
			while ((k<nrOfAllGenes) && (ptr != genePtr[k])) k++;
			interactionsArray[i*nrOfAllGenes+j] = k;
		}
	}
	for (i=nrOfGenes;i<((unsigned int) nrOfAllGenes);++i)
	{
		for (j=0;j<interactionsCounter[i];++j)
		{
			k=0;
			ptr = ((TGeneInteraction*) genesWithPreRNA[i-nrOfGenes]->interactions[j])->interactingGene;
			while ((k<nrOfAllGenes) && (ptr != genePtr[k])) k++;
			interactionsArray[i*nrOfAllGenes+j] = k;
		}
	}

	//Modify the type of gene if has no interactions
	for (i=0;i<((unsigned int) nrOfAllGenes);++i)
	{
		if(interactionsCounter[i] == 0) geneType[i] += 4; //gene with no interaction
		j = interactionsArray[i*nrOfAllGenes];
		if(interactionsCounter[i] == 1)
		{
			if (j >= nrOfAllGenes) geneType[i] += 2; //gene with interaction only with light
		}
	}

	//Initialization of dynamic interaction related arrays
	interactionsCurVal = new (std::nothrow) double [nrOfAllGenes*nrOfAllGenes];
	if (interactionsCurVal == NULL)
	{
		freeArrays();
		return false;
	}
	for (i=0;i<((unsigned int) nrOfAllGenes*nrOfAllGenes);++i) interactionsCurVal[i] = 0.0;

	//Allocate the necessary temporal arrays that contains the necessary data
	//to perform the concentration calculation for several steps.
	//This calculation method allows to determine paralelly many concentration values,
	//because the genes has no recent interactions among them. 
	//All present interactions uses concentration values a-priori determined.
	//The size of arrays usually is higher than the size of recent interaction independent period,
	//but that later value may depend from the in each iteration determined 
	//delay parameters.
	//The array size is safe until no interaction has delay greater than 24h.
	
	//allocate the array of minusSumC, expSumC and Slope arrays
	arrayMinusSumC = new (std::nothrow) double [maxStepsInOneDay+1];
	if (arrayMinusSumC == NULL)
	{
		freeArrays();
		return false;
	}
	
	arrayExpSumC = new (std::nothrow) double [maxStepsInOneDay+1];
	if (arrayExpSumC == NULL)
	{
		freeArrays();
		return false;
	}

	arrayOfSlopes = new (std::nothrow) double [maxStepsInOneDay+1];
	if (arrayOfSlopes == NULL)
	{
		freeArrays();
		return false;
	}

	arrayOfMidSlopes = new (std::nothrow) double [maxStepsInOneDay+1];
	if (arrayOfMidSlopes == NULL)
	{
		freeArrays();
		return false;
	}

	//Allocation of two helper arrays
	cyclesForGenes = (int*) (new (std::nothrow) int [nrOfAllGenes]);
	if (cyclesForGenes == NULL)
	{
		freeArrays();
		return false;
	}

	truncCyclesForGenes = (int*) (new (std::nothrow) int [nrOfAllGenes]);
	if (truncCyclesForGenes == NULL)
	{
		freeArrays();
		return false;
	}

	//Create the oservation related arrays
	arrayNrOfObs = (int*) (new (std::nothrow) int [2*nrOfAllGenes]);
	if (arrayNrOfObs == NULL)
	{
		freeArrays();
		return false;
	}

	obsTimePtr = (int**) (new (std::nothrow) void* [2*nrOfAllGenes]);
	if (obsTimePtr == NULL)
	{
		freeArrays();
		return false;
	}

	obsConcPtr = (double**) (new (std::nothrow) void* [2*nrOfAllGenes]);
	if (obsConcPtr == NULL)
	{
		freeArrays();
		return false;
	}

	//Fill the main observations arrays
	for (i=0;i<((unsigned int) 2*nrOfAllGenes);++i)
	{
		obsTimePtr[i] = NULL;
		obsConcPtr[i] = NULL;
		arrayNrOfObs[i] = 0;
	}

	for (i=0;i<((unsigned int) nrOfGenes);++i)
	{
		arrayNrOfObs[2*i] =  genes[i]->mRNAConcentrations->observedData.size();
		obsTimePtr[2*i] = (int*) (new (std::nothrow) int [arrayNrOfObs[2*i]]);
		if (obsTimePtr[2*i] == NULL)
		{
			freeArrays();
			return false;
		}
		obsConcPtr[2*i] = (double*) (new (std::nothrow) double [arrayNrOfObs[2*i]]);
		if (obsConcPtr[2*i] == NULL)
		{
			freeArrays();
			return false;
		}
	}

	for (i=nrOfGenes;i<((unsigned int) nrOfAllGenes);++i)
	{
		arrayNrOfObs[2*i] =  genesWithPreRNA[i-nrOfGenes]->mRNAConcentrations->observedData.size();
		obsTimePtr[2*i] = (int*) (new (std::nothrow) int [arrayNrOfObs[2*i]]);
		if (obsTimePtr[2*i] == NULL)
		{
			freeArrays();
			return false;
		}
		obsConcPtr[2*i] = (double*) (new (std::nothrow) double [arrayNrOfObs[2*i]]);
		if (obsConcPtr[2*i] == NULL)
		{
			freeArrays();
			return false;
		}

		arrayNrOfObs[2*i+1] =  genesWithPreRNA[i-nrOfGenes]->preRNAConcentrations->observedData.size();
		obsTimePtr[2*i+1] = (int*) (new (std::nothrow) int [arrayNrOfObs[2*i+1]]);
		if (obsTimePtr[2*i+1] == NULL)
		{
			freeArrays();
			return false;
		}
		obsConcPtr[2*i+1] = (double*) (new (std::nothrow) double [arrayNrOfObs[2*i+1]]);
		if (obsConcPtr[2*i+1] == NULL)
		{
			freeArrays();
			return false;
		}
	}

	//fill observation data and determine the maximal observation time
	maxObsTime = -1;
	int* intPtr;
	double* doublePtr;

	for (i=0;i<((unsigned int) nrOfGenes);++i)
	{
		intPtr = obsTimePtr[2*i];
		doublePtr = obsConcPtr[2*i];
		for (j=0;j<arrayNrOfObs[2*i];++j)
		{
			intPtr[j] = genes[i]->mRNAConcentrations->observedData.at(j).first;
			if (intPtr[j] > maxObsTime) maxObsTime = intPtr[j];
			doublePtr[j] = genes[i]->mRNAConcentrations->observedData.at(j).second;
		}
	}
	
	for (i=nrOfGenes;i<((unsigned int) nrOfAllGenes);++i)
	{
		intPtr = obsTimePtr[2*i];
		doublePtr = obsConcPtr[2*i];
		for (j=0;j<arrayNrOfObs[2*i];++j)
		{
			intPtr[j] = genesWithPreRNA[i-nrOfGenes]->mRNAConcentrations->observedData.at(j).first;
			if (intPtr[j] > maxObsTime) maxObsTime = intPtr[j];
			doublePtr[j] = genesWithPreRNA[i-nrOfGenes]->mRNAConcentrations->observedData.at(j).second;
		}
		
		intPtr = obsTimePtr[2*i+1];
		doublePtr = obsConcPtr[2*i+1];
		for (j=0;j<arrayNrOfObs[2*i+1];++j)
		{
			intPtr[j] = genesWithPreRNA[i-nrOfGenes]->preRNAConcentrations->observedData.at(j).first;
			if (intPtr[j] > maxObsTime) maxObsTime = intPtr[j];
			doublePtr[j] = genesWithPreRNA[i-nrOfGenes]->preRNAConcentrations->observedData.at(j).second;
		}
	}

	return TRUE;
}

//Free all allocated array if speed execution is not possible or in the end of execution
void TExtGeneVector::freeArrays()
{
	if (geneType != NULL)
	{
		delete [] geneType;
		geneType = NULL;
	}

	if (concArray != NULL)
	{
		delete [] concArray;
		concArray = NULL;
	}

	if (delayArrayInDeltaT != NULL)
	{
		delete [] delayArrayInDeltaT;
		delayArrayInDeltaT = NULL;
	}

	if (minimumDelayArrayInDeltaT != NULL)
	{
		delete [] minimumDelayArrayInDeltaT;
		minimumDelayArrayInDeltaT = NULL;
	}

	if (currentDegradation != NULL)
	{
		delete [] currentDegradation;
		currentDegradation = NULL;
	}

	if (value_A != NULL)
	{
		delete [] value_A;
		value_A = NULL;
	}

	if (value_minusB != NULL)
	{
		delete [] value_minusB;
		value_minusB = NULL;
	}

	if (value_minusSumC != NULL)
	{
		delete [] value_minusSumC;
		value_minusSumC = NULL;
	}

	if (value_RNAProcessing != NULL)
	{
		delete [] value_RNAProcessing;
		value_RNAProcessing = NULL;
	}

	if (lastSumC != NULL)
	{
		delete [] lastSumC;
		lastSumC = NULL;
	}

	if (lastExpSumC != NULL)
	{
		delete [] lastExpSumC;
		lastExpSumC = NULL;
	}

	if (constSlope != NULL)
	{
		delete [] constSlope;
		constSlope = NULL;
	}

	if (darkConstSlope != NULL)
	{
		delete [] darkConstSlope;
		darkConstSlope = NULL;
	}

	if (lastSlope != NULL)
	{
		delete [] lastSlope;
		lastSlope = NULL;
	}

	if (genePtr != NULL)
	{
		delete [] genePtr;
		genePtr = NULL;
	}

	if (interactionsCounter != NULL)
	{
		delete [] interactionsCounter;
		interactionsCounter = NULL;
	}

	if (interactionsArray != NULL)
	{
		delete [] interactionsArray;
		interactionsArray = NULL;
	}

	if (interactionsCurVal != NULL)
	{
		delete [] interactionsCurVal;
		interactionsCurVal = NULL;
	}

	if (arrayMinusSumC != NULL)
	{
		delete [] arrayMinusSumC;
		arrayMinusSumC = NULL;
	}

	if (arrayExpSumC != NULL)
	{
		delete [] arrayExpSumC;
		arrayExpSumC = NULL;
	}

	if (arrayOfSlopes != NULL)
	{
		delete [] arrayOfSlopes;
		arrayOfSlopes = NULL;
	}

	if (arrayOfMidSlopes != NULL)
	{
		delete [] arrayOfMidSlopes;
		arrayOfMidSlopes = NULL;
	}

	if (cyclesForGenes != NULL)
	{
		delete [] cyclesForGenes;
		cyclesForGenes = NULL;
	}

	if (truncCyclesForGenes != NULL)
	{
		delete [] truncCyclesForGenes;
		truncCyclesForGenes = NULL;
	}

	//observation arrays
	for (int i=0;i<2*nrOfAllGenes;++i)
	{
		if (obsTimePtr[i] != NULL)
		{
			delete [] obsTimePtr[i];
			obsTimePtr[i] = NULL;
		}

		if (obsConcPtr[i] != NULL)
		{
			delete [] obsConcPtr[i];
			obsConcPtr[i] = NULL;
		}
	}

	if (arrayNrOfObs != NULL)
	{
		delete [] arrayNrOfObs;
		arrayNrOfObs = NULL;
	}

	if (obsTimePtr != NULL)
	{
		delete [] obsTimePtr;
		obsTimePtr = NULL;
	}

	if (obsConcPtr != NULL)
	{
		delete [] obsConcPtr;
		obsConcPtr = NULL;
	}
}

//****************************************************************
//Initialization methods
//****************************************************************

void TExtGeneVector::prepareArraysForCalculations()
{
	int i,j,k;

	for (i=0;i<nrOfGenes;++i)
	{
		currentDegradation[i] = genes[i]->degradation.currentValue();
		value_A[i] = genes[i]->maxProduction.currentValue();
		value_minusB[i] = -log(value_A[i]/genes[i]->baseProduction.currentValue());
		delayArrayInDeltaT[i] = genes[i]->delay.currentValue()/deltaT;
	}

	for (i=0;i<nrOfGenesWithPreRNA;++i)
	{
		j = i + nrOfGenes;
		currentDegradation[j] = genesWithPreRNA[i]->degradation.currentValue();
		value_A[j] = genesWithPreRNA[i]->maxProduction.currentValue();
		value_minusB[j] = -log(value_A[j]/genesWithPreRNA[i]->baseProduction.currentValue());
		value_RNAProcessing[i] = genesWithPreRNA[i]->RNAProcessing.currentValue();
		delayArrayInDeltaT[j] = genesWithPreRNA[i]->delay.currentValue()/deltaT;
	}

	//Determine the minimal delay in deltaT
	minimalDelayInDeltaT = sizeOfConcArray;
	maximalDelayInDeltaT = 0;
	for (i=0;i<nrOfAllGenes;++i)
	{
		if (minimalDelayInDeltaT > delayArrayInDeltaT[i]) 
			minimalDelayInDeltaT = delayArrayInDeltaT[i];
		if (maximalDelayInDeltaT < delayArrayInDeltaT[i]) 
			maximalDelayInDeltaT = delayArrayInDeltaT[i];
	}

	if (lightLimitInDeltaT > 0)
		if (minimalDelayInDeltaT > (unsigned int) lightLimitInDeltaT)
			minimalDelayInDeltaT = lightLimitInDeltaT;
	
	TmRNAWithPreRNA* InvestigatedGene;
	int intSize;
	double minusSumC;
	double expMidC;
	double curVal;
	int helperIdx;

	//Initialize several helper arrays
	for (i=0;i<nrOfAllGenes;++i)
	{
		minimumDelayArrayInDeltaT[i] = sizeOfConcArray;
		intSize = interactionsCounter[i];
		minusSumC = 0.0;
		InvestigatedGene = (TmRNAWithPreRNA*) genePtr[i];
		helperIdx = i*nrOfAllGenes;
		for (j=0;j<intSize;++j)
		{
			curVal = InvestigatedGene->interactions[j]->c.curVal;
			k = interactionsArray[helperIdx+j];
			if (k<nrOfAllGenes)
			{
				interactionsCurVal[helperIdx+k] = curVal;
				if (minimumDelayArrayInDeltaT[i] > delayArrayInDeltaT[k]) minimumDelayArrayInDeltaT[i] = delayArrayInDeltaT[k];

				if (k >= nrOfGenes)
					minusSumC -= curVal * concArray[(2*k-nrOfGenes)*sizeOfConcArray+1];
				else
					minusSumC -= curVal * concArray[k*sizeOfConcArray+1];
			}
			else
			{
				minusSumC -= curVal;
			}
		}

		value_minusSumC[i] = minusSumC;
		lastSumC[i] = minusSumC;
		expMidC = exp(minusSumC);
		lastExpSumC[i] =  expMidC;
		constSlope[i] = value_A[i] * exp(value_minusB[i] * expMidC);
		lastSlope[i] = constSlope[i];
		darkConstSlope[i] = value_A[i] * exp(value_minusB[i]);
	}
}

//****************************************************************
//Processing methods
//****************************************************************

void TExtGeneVector::executeInteractionFreePhase()
{
	int i;
	unsigned int j;

	//degradation related variables
	double degrad;
	double degradDeltaT;
	double halfDegradDeltaT;
	double halfDegradDeltaT2;
	double oneMinusHalfDegradDeltaT;
	double degradDeltaT2;

	//slope related variables
	double slope;
	double slopeDeltaT;

	//concentration related variables
	double k1;
	double k1mRNA;
	double concCalcCoef;	//it's value can be deducted from the eq. k1..k4 and next conc. eq., having ct. slope
	double curConcRNA;
	double curConcPreRNA;
	double h1;						//helper variable

	//helper variables
	int stepInArray;
	int stepInArray2;
	int stepInArrayPlus1;
	int stepInArray2Plus1;

	//for genes with preRNA
	double rnaProcessing;
	double rnaProcessingDeltaT;
	double rnaProcessingDeltaT2;
	double concCalcCoef2;  //it's value can be deducted from the eq. k1..k4, eq. k1mRNA..k4mRNA and next conc. eq., having ct. slope
	double concCalcCoef3;  //it's value can be deducted from the eq. k1..k4, eq. k1mRNA..k4mRNA and next conc. eq., having ct. slope

	
	//for normal genes
	stepInArray = maxStepsInOneDay - sizeOfConcArray;
	for (i=0;i<nrOfGenes;++i)
	{
		degrad = currentDegradation[i];
		degradDeltaT = deltaT * degrad;
		halfDegradDeltaT = halfDeltaT * degrad;
		halfDegradDeltaT2 = halfDegradDeltaT*halfDegradDeltaT;
		oneMinusHalfDegradDeltaT = 1.0 - halfDegradDeltaT;
		concCalcCoef = oneMinusHalfDegradDeltaT + twoThird*halfDegradDeltaT2 - oneThird*halfDegradDeltaT2*halfDegradDeltaT;
		stepInArray += sizeOfConcArray;
		stepInArrayPlus1 = stepInArray + 1;
		
		slope = constSlope[i];
		slopeDeltaT = deltaT * slope;

		curConcRNA = concArray[stepInArray];
		for (j=0;j<minimalDelayInDeltaT;++j)
		{
			k1 = slopeDeltaT - degradDeltaT * curConcRNA;
			curConcRNA += k1*concCalcCoef;
			concArray[stepInArrayPlus1+j] = curConcRNA;
		}

		//Test if the values are correct
		//It seems to be strange, but the expression concentration != concentration is FALSE in case of curConcRNA = NaN Not a number value!!!
		if ((curConcRNA > highestPossibleConcentration) || (curConcRNA != curConcRNA)) correctConcentrationValues = FALSE;  
		else
			if ((curConcRNA < lowestPossibleConcentration) && (curConcRNA != -1.0)) correctConcentrationValues = FALSE;
		if (correctConcentrationValues == FALSE) return;
	}

	//for genes with preRNA
	stepInArray = (nrOfGenes-2)*sizeOfConcArray + maxStepsInOneDay;
	stepInArray2 = stepInArray + sizeOfConcArray;

	for (i=nrOfGenes;i<nrOfAllGenes;++i)
	{
	  rnaProcessing = value_RNAProcessing[i-nrOfGenes];
		stepInArray += twiceSizeOfConcArray;
		stepInArray2 = stepInArray + sizeOfConcArray;
		stepInArrayPlus1 = stepInArray + 1;
		stepInArray2Plus1 = stepInArray2 + 1;

		degrad = currentDegradation[i];
		slope = constSlope[i]; 
		slopeDeltaT = deltaT * slope;

		degradDeltaT = deltaT * degrad;
		degradDeltaT2 = degradDeltaT * degradDeltaT;
		rnaProcessingDeltaT = deltaT * rnaProcessing;
		rnaProcessingDeltaT2 = rnaProcessingDeltaT * rnaProcessingDeltaT;
		halfDegradDeltaT = halfDeltaT * rnaProcessing;
		halfDegradDeltaT2 = halfDegradDeltaT*halfDegradDeltaT;
		oneMinusHalfDegradDeltaT = 1.0 - halfDegradDeltaT;

		concCalcCoef = oneMinusHalfDegradDeltaT + twoThird*halfDegradDeltaT2 - oneThird*halfDegradDeltaT2*halfDegradDeltaT;
		concCalcCoef2 = 1.0 - oneSixth*(3.0*degradDeltaT - degradDeltaT2 + 0.25*degradDeltaT2*degradDeltaT);
		concCalcCoef3 = oneSixth*(3.0*rnaProcessingDeltaT - rnaProcessingDeltaT2 - rnaProcessingDeltaT*degradDeltaT
			+ 0.25*(rnaProcessingDeltaT2*rnaProcessingDeltaT + rnaProcessingDeltaT2*degradDeltaT + rnaProcessingDeltaT*degradDeltaT2));
	
		curConcRNA = concArray[stepInArray];
		curConcPreRNA = concArray[stepInArray2];
		for (j=0;j<minimalDelayInDeltaT;++j)
		{
			//Update mRNA and preRNA
			h1 = rnaProcessingDeltaT * curConcPreRNA;
			k1 = slopeDeltaT - h1;
			k1mRNA = h1 - degradDeltaT * curConcRNA;
			curConcPreRNA += k1*concCalcCoef;
			curConcRNA += k1*concCalcCoef3 + k1mRNA*concCalcCoef2;
			concArray[stepInArray2Plus1+j] = curConcPreRNA;
			concArray[stepInArrayPlus1+j] = curConcRNA;
		}

		//Test if the values are correct
		//It seems to be strange, but the expression concentration != concentration is FALSE in case of curConcRNA = NaN Not a number value!!!
		if ((curConcRNA > highestPossibleConcentration) || (curConcRNA != curConcRNA)) correctConcentrationValues = FALSE;  
		else
			if ((curConcRNA < lowestPossibleConcentration) && (curConcRNA != -1.0)) correctConcentrationValues = FALSE;
		if ((curConcPreRNA > highestPossibleConcentration) || (curConcPreRNA != curConcPreRNA)) correctConcentrationValues = FALSE;  
		else
			if ((curConcPreRNA < lowestPossibleConcentration) && (curConcPreRNA != -1.0)) correctConcentrationValues = FALSE;
		if (correctConcentrationValues == FALSE) return;
	}
}

void TExtGeneVector::executeInteractionFreePhaseForOneGene(int geneNr, int startPosition, int endPosition)
{
	int i;

	//degradation related variables
	double degrad;
	double degradDeltaT;
	double halfDegradDeltaT;
	double halfDegradDeltaT2;
	double oneMinusHalfDegradDeltaT;
	double degradDeltaT2;

	//slope related parameters 
	double slope;
	double slopeDeltaT;
	
	//concentration related variables
	double k1,k3,k4;
	double k1mRNA, k2mRNA, k3mRNA, k4mRNA;
	double concCalcCoef;	//it's value can be deducted from the eq. k1..k4 and next conc. eq., having ct. slope
	double curConcRNA;
	double curConcPreRNA;
	double h1,h2;						//helper variable

	//gene type related variables
	int currentGeneType;

	//for genes with preRNA
	double rnaProcessing; 
	double rnaProcessingDeltaT;
	double rnaProcessingDeltaT2;
	double concCalcCoef2;  //it's value can be deducted from the eq. k1..k4, eq. k1mRNA..k4mRNA and next conc. eq., having ct. slope
	double concCalcCoef3;  //it's value can be deducted from the eq. k1..k4, eq. k1mRNA..k4mRNA and next conc. eq., having ct. slope

	//helper variables
	int stepInArray;
	int stepInArray2;
	int stepInArrayPlus1;
	int stepInArray2Plus1;

	//light related parameters
	int ftpAbsolute;
	int ftpAbsolute2;
	int startPositionInDay;
	int endPositionInDay;
	int lightLimitInDeltaTMinusOne;

	//for all gene types
	degrad = currentDegradation[geneNr];
	degradDeltaT = deltaT * degrad;

	slope = constSlope[geneNr]; 
	slopeDeltaT = deltaT * slope;
	currentGeneType = geneType[geneNr];

	if ((currentGeneType == 1) || (currentGeneType == 5))
	{
		//for normal gene without light	interaction
		halfDegradDeltaT = halfDeltaT * degrad;
		halfDegradDeltaT2 = halfDegradDeltaT*halfDegradDeltaT;
		oneMinusHalfDegradDeltaT = 1.0 - halfDegradDeltaT;
		concCalcCoef = oneMinusHalfDegradDeltaT + twoThird*halfDegradDeltaT2 - oneThird*halfDegradDeltaT2*halfDegradDeltaT;

		stepInArray = geneNr * sizeOfConcArray + maxStepsInOneDay;
		stepInArrayPlus1 = stepInArray + 1;

		curConcRNA = concArray[stepInArray+startPosition];
		for (i=startPosition;i<endPosition;++i)
		{
			k1 = slopeDeltaT - degradDeltaT * curConcRNA;
			curConcRNA += k1*concCalcCoef;
			concArray[stepInArrayPlus1+i] = curConcRNA;
		}
		//Test if the values are correct
		//It seems to be strange, but the expression concentration != concentration is FALSE in case of curConcRNA = NaN Not a number value!!!
		if ((curConcRNA > highestPossibleConcentration) || (curConcRNA != curConcRNA)) correctConcentrationValues = FALSE;  
		else
			if ((curConcRNA < lowestPossibleConcentration) && (curConcRNA != -1.0)) correctConcentrationValues = FALSE;
		if (correctConcentrationValues == FALSE) return;
	}

	if ((currentGeneType == 2) || (currentGeneType == 6))
	{
		//for genes with preRNA without light	interaction
		rnaProcessing = value_RNAProcessing[geneNr-nrOfGenes];
		stepInArray = (2*geneNr-nrOfGenes)*sizeOfConcArray + maxStepsInOneDay;
		stepInArray2 = stepInArray + sizeOfConcArray;
		stepInArrayPlus1 = stepInArray + 1;
		stepInArray2Plus1 = stepInArray2 + 1;

		degradDeltaT2 = degradDeltaT * degradDeltaT;		
		rnaProcessingDeltaT = deltaT * rnaProcessing;
		rnaProcessingDeltaT2 = rnaProcessingDeltaT * rnaProcessingDeltaT;
		halfDegradDeltaT = halfDeltaT * rnaProcessing;
		halfDegradDeltaT2 = halfDegradDeltaT*halfDegradDeltaT;
		oneMinusHalfDegradDeltaT = 1.0 - halfDegradDeltaT;
		
		concCalcCoef = oneMinusHalfDegradDeltaT + twoThird*halfDegradDeltaT2 - oneThird*halfDegradDeltaT2*halfDegradDeltaT;
		concCalcCoef2 = 1.0 - oneSixth*(3.0*degradDeltaT - degradDeltaT2 + 0.25*degradDeltaT2*degradDeltaT);
		concCalcCoef3 = oneSixth*(3.0*rnaProcessingDeltaT - rnaProcessingDeltaT2 - rnaProcessingDeltaT*degradDeltaT
			+ 0.25*(rnaProcessingDeltaT2*rnaProcessingDeltaT + rnaProcessingDeltaT2*degradDeltaT + rnaProcessingDeltaT*degradDeltaT2));

		curConcRNA = concArray[stepInArray+startPosition];
		curConcPreRNA = concArray[stepInArray2+startPosition];

		for (i=startPosition;i<endPosition;++i)
		{
			//Update mRNA and preRNA
			h1 = rnaProcessingDeltaT * curConcPreRNA;
			k1 = slopeDeltaT - h1;
			k1mRNA = h1 - degradDeltaT * curConcRNA;
			curConcPreRNA += k1*concCalcCoef;
			curConcRNA += k1*concCalcCoef3 + k1mRNA*concCalcCoef2;
			concArray[stepInArray2Plus1+i] = curConcPreRNA;
			concArray[stepInArrayPlus1+i] = curConcRNA;
		}
		//Test if the values are correct
		//It seems to be strange, but the expression concentration != concentration is FALSE in case of curConcRNA = NaN Not a number value!!!
		if ((curConcRNA > highestPossibleConcentration) || (curConcRNA != curConcRNA)) correctConcentrationValues = FALSE;  
		else
			if ((curConcRNA < lowestPossibleConcentration) && (curConcRNA != -1.0)) correctConcentrationValues = FALSE;
		if ((curConcPreRNA > highestPossibleConcentration) || (curConcPreRNA != curConcPreRNA)) correctConcentrationValues = FALSE;  
		else
			if ((curConcPreRNA < lowestPossibleConcentration) && (curConcPreRNA != -1.0)) correctConcentrationValues = FALSE;
		if (correctConcentrationValues == FALSE) return;
	}

	if (currentGeneType == 3)
	{
		//for normal gene with only Light interaction
		halfDegradDeltaT = halfDeltaT * degrad;
		halfDegradDeltaT2 = halfDegradDeltaT*halfDegradDeltaT;
		oneMinusHalfDegradDeltaT = 1.0 - halfDegradDeltaT;
		concCalcCoef = oneThird*(3.0*oneMinusHalfDegradDeltaT + 2.0*halfDegradDeltaT2 - halfDegradDeltaT2*halfDegradDeltaT); 
		stepInArray = geneNr*sizeOfConcArray + maxStepsInOneDay;
		stepInArrayPlus1 = stepInArray + 1;

		startPositionInDay = startPosition % maxStepsInOneDay;
		endPositionInDay = endPosition - startPosition + startPositionInDay;
		ftpAbsolute = startPosition - startPositionInDay + lightLimitInDeltaT;
		ftpAbsolute2 = startPosition - startPositionInDay + maxStepsInOneDay;

		curConcRNA = concArray[stepInArray+startPosition];

		if (endPositionInDay < lightLimitInDeltaT)
		{
			//light is on
			for (i=startPosition;i<endPosition;++i)
			{
				k1 = slopeDeltaT - degradDeltaT * curConcRNA;
				curConcRNA += k1*concCalcCoef;
				concArray[stepInArrayPlus1+i] = curConcRNA;
			}
		}
		else
		{
			if (startPositionInDay < lightLimitInDeltaT)
			{
				//the light will be switched off
				lightLimitInDeltaTMinusOne = ftpAbsolute - 1;
				for (i=startPosition;i<lightLimitInDeltaTMinusOne;++i)
				{
					k1 = slopeDeltaT - degradDeltaT * curConcRNA;
					curConcRNA += k1*concCalcCoef;
					concArray[stepInArrayPlus1+i] = curConcRNA;
				}

				//the last step is important
				k1 = slopeDeltaT - degradDeltaT * curConcRNA;
				h2 = (2.0 * oneMinusHalfDegradDeltaT + halfDegradDeltaT2) * k1;
				slope = darkConstSlope[geneNr];
				k3 = h2 - k1 * oneMinusHalfDegradDeltaT;
				k4 = deltaT * (slope - degrad * (curConcRNA + k3));
				curConcRNA += oneSixth*(k1+k4) + oneThird*(h2);
				concArray[stepInArrayPlus1+lightLimitInDeltaTMinusOne] = curConcRNA;

				//execute auxiliar steps if exists
				slopeDeltaT = deltaT * slope;
				for (i=ftpAbsolute;i<endPosition;++i)
				{
					k1 = slopeDeltaT - degradDeltaT * curConcRNA;
					curConcRNA += k1*concCalcCoef;
					concArray[stepInArrayPlus1+i] = curConcRNA;
				}
			}
			else
			{
				//light is off
				slope = darkConstSlope[geneNr];
				slopeDeltaT = deltaT * slope;
				if (endPositionInDay >= maxStepsInOneDay)
				{
					//the light will be switched on
					lightLimitInDeltaTMinusOne = ftpAbsolute2 - 1;
					for (i=startPosition;i<lightLimitInDeltaTMinusOne;++i)
					{
						k1 = slopeDeltaT - degradDeltaT * curConcRNA;
						curConcRNA += k1*concCalcCoef;
						concArray[stepInArrayPlus1+i] = curConcRNA;
					}

					//the last step is important
					k1 = slopeDeltaT - degradDeltaT * curConcRNA;
					h2 = (2.0 * oneMinusHalfDegradDeltaT + halfDegradDeltaT2) * k1;
					slope = constSlope[geneNr];
					k3 = h2 - k1 * oneMinusHalfDegradDeltaT;
					k4 = deltaT * (slope - degrad * (curConcRNA + k3));
					curConcRNA += oneSixth*(k1+k4) + oneThird*(h2);
					concArray[stepInArrayPlus1+lightLimitInDeltaTMinusOne] = curConcRNA;

					//execute auxiliar steps if exists
					slopeDeltaT = deltaT * slope;
					for (i=ftpAbsolute2;i<endPosition;++i)
					{
						k1 = slopeDeltaT - degradDeltaT * curConcRNA;
						curConcRNA += k1*concCalcCoef;
						concArray[stepInArrayPlus1+i] = curConcRNA;
					}
				}
				else
				{
					//light will remain off
					for (i=startPosition;i<endPosition;++i)
					{
						k1 = slopeDeltaT - degradDeltaT * curConcRNA;
						curConcRNA += k1*concCalcCoef;
						concArray[stepInArrayPlus1+i] = curConcRNA;
					}
				}
			}
		}

		//Test if the values are correct
		//It seems to be strange, but the expression concentration != concentration is FALSE in case of curConcRNA = NaN Not a number value!!!
		if ((curConcRNA > highestPossibleConcentration) || (curConcRNA != curConcRNA)) correctConcentrationValues = FALSE;  
		else
			if ((curConcRNA < lowestPossibleConcentration) && (curConcRNA != -1.0)) correctConcentrationValues = FALSE;
		if (correctConcentrationValues == FALSE) return;
	}

	if (currentGeneType == 4)
	{
		//for genes with preRNA with only Light interaction
		rnaProcessing = value_RNAProcessing[geneNr-nrOfGenes];
		stepInArray = (2*geneNr-nrOfGenes)*sizeOfConcArray + maxStepsInOneDay;
		stepInArray2 = stepInArray + sizeOfConcArray;
		stepInArrayPlus1 = stepInArray + 1;
		stepInArray2Plus1 = stepInArray2 + 1;

		degradDeltaT2 = degradDeltaT * degradDeltaT;		
		rnaProcessingDeltaT = deltaT * rnaProcessing;
		rnaProcessingDeltaT2 = rnaProcessingDeltaT * rnaProcessingDeltaT;
		halfDegradDeltaT = halfDeltaT * rnaProcessing;
		halfDegradDeltaT2 = halfDegradDeltaT*halfDegradDeltaT;
		oneMinusHalfDegradDeltaT = 1.0 - halfDegradDeltaT;
		
		concCalcCoef = oneMinusHalfDegradDeltaT + twoThird*halfDegradDeltaT2 - oneThird*halfDegradDeltaT2*halfDegradDeltaT;
		concCalcCoef2 = 1.0 - oneSixth*(3.0*degradDeltaT - degradDeltaT2 + 0.25*degradDeltaT2*degradDeltaT);
		concCalcCoef3 = oneSixth*(3.0*rnaProcessingDeltaT - rnaProcessingDeltaT2 - rnaProcessingDeltaT*degradDeltaT
			+ 0.25*(rnaProcessingDeltaT2*rnaProcessingDeltaT + rnaProcessingDeltaT2*degradDeltaT + rnaProcessingDeltaT*degradDeltaT2));

		startPositionInDay = startPosition % maxStepsInOneDay;
		endPositionInDay = endPosition - startPosition + startPositionInDay;
		ftpAbsolute = startPosition - startPositionInDay + lightLimitInDeltaT;
		ftpAbsolute2 = startPosition - startPositionInDay + maxStepsInOneDay;

		curConcRNA = concArray[stepInArray+startPosition];
		curConcPreRNA = concArray[stepInArray2+startPosition];

		if (endPositionInDay < lightLimitInDeltaT)
		{
			//light is on
			for (i=startPosition;i<endPosition;++i)
			{
				//Update mRNA and preRNA
				h1 = rnaProcessingDeltaT * curConcPreRNA;
				k1 = slopeDeltaT - h1;
				k1mRNA = h1 - degradDeltaT * curConcRNA;
				curConcPreRNA += k1*concCalcCoef;
				curConcRNA += k1*concCalcCoef3 + k1mRNA*concCalcCoef2;
				concArray[stepInArray2Plus1+i] = curConcPreRNA;
				concArray[stepInArrayPlus1+i] = curConcRNA;
			}
		}
		else
		{
			if (startPositionInDay < lightLimitInDeltaT)
			{
				//the light will be switched off
				lightLimitInDeltaTMinusOne = ftpAbsolute - 1;
				for (i=startPosition;i<lightLimitInDeltaTMinusOne;++i)
				{
					//Update mRNA and preRNA
					h1 = rnaProcessingDeltaT * curConcPreRNA;
					k1 = slopeDeltaT - h1;
					k1mRNA = h1 - degradDeltaT * curConcRNA;
					curConcPreRNA += k1*concCalcCoef;
					curConcRNA += k1*concCalcCoef3 + k1mRNA*concCalcCoef2;
					concArray[stepInArray2Plus1+i] = curConcPreRNA;
					concArray[stepInArrayPlus1+i] = curConcRNA;
				}
				
				h1 = rnaProcessingDeltaT * curConcPreRNA;
				k1 = slopeDeltaT - h1;
				h2 = (2.0 - rnaProcessingDeltaT + 0.25 * rnaProcessingDeltaT2) * k1;
				slope = darkConstSlope[geneNr];
				slopeDeltaT = deltaT * slope;
				k3 = h2 - k1 * (1.0 - 0.5 * rnaProcessingDeltaT);	
				k4 = slopeDeltaT - h1 - rnaProcessingDeltaT*k3;
			
				k1mRNA = h1 - degradDeltaT * curConcRNA;
				k2mRNA = k1mRNA*(1.0 - 0.5*degradDeltaT) + 0.5 * rnaProcessingDeltaT * k1;
				k3mRNA = deltaT * (rnaProcessing * (curConcPreRNA + 0.5 * k1 * (1 - 0.5 * rnaProcessingDeltaT)) - degrad * (curConcRNA + 0.5 * k2mRNA));
				k4mRNA = deltaT * (rnaProcessing * (curConcPreRNA + k3) - degrad * (curConcRNA + k3mRNA));

				curConcPreRNA += oneSixth*(k1+k4) + oneThird*(h2);
				curConcRNA += oneSixth*(k1mRNA+k4mRNA) + oneThird*(k2mRNA + k3mRNA);
				concArray[stepInArray2Plus1+lightLimitInDeltaTMinusOne] = curConcPreRNA;
				concArray[stepInArrayPlus1+lightLimitInDeltaTMinusOne] = curConcRNA; 
				
				//execute auxiliar steps if exists
				for (i=ftpAbsolute;i<endPosition;++i)
				{
					//Update mRNA and preRNA
					h1 = rnaProcessingDeltaT * curConcPreRNA;
					k1 = slopeDeltaT - h1;
					k1mRNA = h1 - degradDeltaT * curConcRNA;
					curConcPreRNA += k1*concCalcCoef;
					curConcRNA += k1*concCalcCoef3 + k1mRNA*concCalcCoef2;
					concArray[stepInArray2Plus1+i] = curConcPreRNA;
					concArray[stepInArrayPlus1+i] = curConcRNA;
				}
			}
			else
			{
				//light is off
				slope = darkConstSlope[geneNr];
				slopeDeltaT = deltaT * slope;
				if (endPositionInDay >= maxStepsInOneDay)
				{
					//the light will be switched on
					lightLimitInDeltaTMinusOne = ftpAbsolute2 - 1;
					for (i=startPosition;i<lightLimitInDeltaTMinusOne;++i)
					{
						//Update mRNA and preRNA
						h1 = rnaProcessingDeltaT * curConcPreRNA;
						k1 = slopeDeltaT - h1;
						k1mRNA = h1 - degradDeltaT * curConcRNA;
						curConcPreRNA += k1*concCalcCoef;
						curConcRNA += k1*concCalcCoef3 + k1mRNA*concCalcCoef2;
						concArray[stepInArray2Plus1+i] = curConcPreRNA;
						concArray[stepInArrayPlus1+i] = curConcRNA;
					}

					//the last step is important
					h1 = rnaProcessingDeltaT * curConcPreRNA;
					k1 = slopeDeltaT - h1;
					h2 = (2.0 - rnaProcessingDeltaT + 0.25 * rnaProcessingDeltaT2) * k1;
					slope = constSlope[geneNr];
					slopeDeltaT = deltaT * slope;
					k3 = h2 - k1 * (1.0 - 0.5 * rnaProcessingDeltaT);	
					k4 = slopeDeltaT - h1 - rnaProcessingDeltaT*k3;
			
					k1mRNA = h1 - degradDeltaT * curConcRNA;
					k2mRNA = k1mRNA*(1.0 - 0.5*degradDeltaT) + 0.5 * rnaProcessingDeltaT * k1;
					k3mRNA = deltaT * (rnaProcessing * (curConcPreRNA + 0.5 * k1 * (1 - 0.5 * rnaProcessingDeltaT)) - degrad * (curConcRNA + 0.5 * k2mRNA));
					k4mRNA = deltaT * (rnaProcessing * (curConcPreRNA + k3) - degrad * (curConcRNA + k3mRNA));

					curConcPreRNA += oneSixth*(k1+k4) + oneThird*(h2);
					curConcRNA += oneSixth*(k1mRNA+k4mRNA) + oneThird*(k2mRNA + k3mRNA);
					concArray[stepInArray2Plus1+lightLimitInDeltaTMinusOne] = curConcPreRNA;
					concArray[stepInArrayPlus1+lightLimitInDeltaTMinusOne] = curConcRNA; 

					//execute auxiliar steps if exists
					for (i=ftpAbsolute2;i<endPosition;++i)
					{
						//Update mRNA and preRNA
						h1 = rnaProcessingDeltaT * curConcPreRNA;
						k1 = slopeDeltaT - h1;
						k1mRNA = h1 - degradDeltaT * curConcRNA;
						curConcPreRNA += k1*concCalcCoef;
						curConcRNA += k1*concCalcCoef3 + k1mRNA*concCalcCoef2;
						concArray[stepInArray2Plus1+i] = curConcPreRNA;
						concArray[stepInArrayPlus1+i] = curConcRNA;
					}
				}
				else
				{
					//light will remain off
					for (i=startPosition;i<endPosition;++i)
					{
						//Update mRNA and preRNA
						h1 = rnaProcessingDeltaT * curConcPreRNA;
						k1 = slopeDeltaT - h1;
						k1mRNA = h1 - degradDeltaT * curConcRNA;
						curConcPreRNA += k1*concCalcCoef;
						curConcRNA += k1*concCalcCoef3 + k1mRNA*concCalcCoef2;
						concArray[stepInArray2Plus1+i] = curConcPreRNA;
						concArray[stepInArrayPlus1+i] = curConcRNA;
					}
				}
			}
		}	

		//Test if the values are correct
		//It seems to be strange, but the expression concentration != concentration is FALSE in case of curConcRNA = NaN Not a number value!!!
		if ((curConcRNA > highestPossibleConcentration) || (curConcRNA != curConcRNA)) correctConcentrationValues = FALSE;  
		else
			if ((curConcRNA < lowestPossibleConcentration) && (curConcRNA != -1.0)) correctConcentrationValues = FALSE;
		if ((curConcPreRNA > highestPossibleConcentration) || (curConcPreRNA != curConcPreRNA)) correctConcentrationValues = FALSE;  
		else
			if ((curConcPreRNA < lowestPossibleConcentration) && (curConcPreRNA != -1.0)) correctConcentrationValues = FALSE;
		if (correctConcentrationValues == FALSE) return;
	}
}

void TExtGeneVector::executeInteractionActivePhaseForOneGene(int geneNr, int startPosition, int endPosition)
{
	int i,j,k,l;

	//helper variables
	int stepInArray, stepInArray2;
	int intSize = interactionsCounter[geneNr];
	int gapPos, gapPos2;

	//Initialize the semi-constant variables
	double degrad = currentDegradation[geneNr];
	double curConcRNA;
	double curConcPreRNA;
	double rnaProcessing;
	double A = value_A[geneNr];
	double minusB = value_minusB[geneNr];
	double minusSum_c;
	double curVal;
	double intConc;

	//Intermediate values
	double k1,k2,k3,k4;
	double k1mRNA,k2mRNA,k3mRNA,k4mRNA;

	//Fill the array of minusSumC with proper values
	int cycleSize = endPosition - startPosition;
	int cycleSizeP1 = cycleSize + 1;
	int gap = geneNr*nrOfAllGenes;
	int gap2;
	
	//Initialization to allow safe value checking later
	curConcRNA = 1.0;
	curConcPreRNA = 1.0;

	//Initialize the SumC array
	memset(arrayMinusSumC,0x00,cycleSizeP1*sizeof(double));
	
	//Determine the necessary minusSumC values
	arrayMinusSumC[0] = lastSumC[geneNr];
	for (j=0;j<intSize;++j)
	{
		k = interactionsArray[gap+j];
		curVal = interactionsCurVal[gap+k];
		if (k > nrOfGenes) l = 2*k-nrOfGenes; else l = k;
		gap2 = l*sizeOfConcArray-delayArrayInDeltaT[k]+maxStepsInOneDay+startPosition;
		
		if (delayArrayInDeltaT[k]>(unsigned int) endPosition)
		{
			intConc = concArray[gap2];
			minusSum_c = curVal * intConc;
			for (i=1;i<cycleSizeP1;++i) arrayMinusSumC[i] -= minusSum_c;
		}
		else
		{
			for (i=1;i<cycleSizeP1;++i)
			{
				intConc = concArray[gap2+i];
				arrayMinusSumC[i] -= curVal * intConc;
			}
		}
	}
	lastSumC[geneNr] = arrayMinusSumC[cycleSize];

	//Determine the slope values
	double expSumC;
	arrayOfSlopes[0] = lastSlope[geneNr];
	arrayExpSumC[0] = lastExpSumC[geneNr];
	for (i=1;i<cycleSizeP1;++i)
	{
		expSumC = exp(arrayMinusSumC[i]);
		arrayOfSlopes[i] = A * exp(minusB * expSumC);
		arrayExpSumC[i] = expSumC;
		arrayOfMidSlopes[i-1] = A * exp(minusB * sqrt(expSumC*arrayExpSumC[i-1]));
	}
	lastExpSumC[geneNr] = arrayExpSumC[cycleSize];
	lastSlope[geneNr] = arrayOfSlopes[cycleSize];

	//Determine the concentration values
	if (geneNr < nrOfGenes)
	{
		stepInArray = geneNr * sizeOfConcArray + maxStepsInOneDay;
		gapPos = stepInArray+startPosition;
		for (i=0;i<cycleSize;++i)
		{
			curConcRNA = concArray[gapPos + i];
			k1 = deltaT * (arrayOfSlopes[i] - degrad * curConcRNA);
			k2 = deltaT * (arrayOfMidSlopes[i] - degrad * (curConcRNA + 0.5 * k1));
			k3 = deltaT * (arrayOfMidSlopes[i] - degrad * (curConcRNA + 0.5 * k2));
			k4 = deltaT * (arrayOfSlopes[i+1] - degrad * (curConcRNA + k3));
			concArray[gapPos+1+i] = curConcRNA + oneSixth*(k1+k4) + oneThird*(k2 + k3);
		}
	}
	else
	{
		rnaProcessing = value_RNAProcessing[geneNr-nrOfGenes];
		stepInArray = (2*geneNr-nrOfGenes) * sizeOfConcArray + maxStepsInOneDay;
		stepInArray2 = stepInArray + sizeOfConcArray;

		gapPos = stepInArray+startPosition;
		gapPos2 = stepInArray2+startPosition;
		for (i=0;i<cycleSize;++i)
		{
			curConcRNA = concArray[gapPos + i];
			curConcPreRNA = concArray[gapPos2+i];

			k1 = deltaT * (arrayOfSlopes[i] - rnaProcessing * curConcPreRNA);
			k2 = deltaT * (arrayOfMidSlopes[i] - rnaProcessing * (curConcPreRNA + 0.5 * k1));
			k3 = deltaT * (arrayOfMidSlopes[i] - rnaProcessing * (curConcPreRNA + 0.5 * k2));
			k4 = deltaT * (arrayOfSlopes[i+1] - rnaProcessing * (curConcPreRNA + k3));
			
			k1mRNA = deltaT * (rnaProcessing * curConcPreRNA - degrad * curConcRNA);
			k2mRNA = deltaT * (rnaProcessing * (curConcPreRNA + 0.5 * k1) - degrad * (curConcRNA + 0.5 * k1mRNA));
			k3mRNA = deltaT * (rnaProcessing * (curConcPreRNA + 0.5 * k2) - degrad * (curConcRNA + 0.5 * k2mRNA));
			k4mRNA = deltaT * (rnaProcessing * (curConcPreRNA + k3) - degrad * (curConcRNA + k3mRNA));

			concArray[gapPos2+1+i] = curConcPreRNA + oneSixth*(k1+k4) + oneThird*(k2 + k3);
			concArray[gapPos+1+i] = curConcRNA + oneSixth*(k1mRNA+k4mRNA) + oneThird*(k2mRNA + k3mRNA);
		}
	}

	//Test if the values are correct
	//It seems to be strange, but the expression concentration != concentration is FALSE in case of curConcRNA = NaN Not a number value!!!
	if ((curConcRNA > highestPossibleConcentration) || (curConcRNA != curConcRNA)) correctConcentrationValues = FALSE;  
	else
		if ((curConcRNA < lowestPossibleConcentration) && (curConcRNA != -1.0)) correctConcentrationValues = FALSE;
	if ((curConcPreRNA > highestPossibleConcentration) || (curConcPreRNA != curConcPreRNA)) correctConcentrationValues = FALSE;  
	else
		if ((curConcPreRNA < lowestPossibleConcentration) && (curConcPreRNA != -1.0)) correctConcentrationValues = FALSE;
}

void TExtGeneVector::executeInPresenceOfActiveInteractions()
{
	int i,j;

	//determine the cycle parameters
	int maxStepInOneCycle = minimalDelayInDeltaT;
	
	//Speedup: do not calculate unused values
	int totalSimulationSteps = sizeOfConcArray - 2 * maxStepsInOneDay +  maxObsTime + 1;
	int cycles = totalSimulationSteps/maxStepInOneCycle;
	//int truncatedCycleLength = totalSimulationSteps - cycles*maxStepInOneCycle;
	//int cycles = 1 + (sizeOfConcArray-maxStepsInOneDay-maxStepInOneCycle)/maxStepInOneCycle; //one cycle executed in executeInteractionFreePhase
	//int truncatedCycleLength = (sizeOfConcArray-maxStepsInOneDay-maxStepInOneCycle) - cycles*maxStepInOneCycle;
	int startPosition = 0;
	int endPosition = maxStepInOneCycle;
	int truncPosition;

	for (i=0;i<nrOfAllGenes;++i)
	{
		cyclesForGenes[i] = minimumDelayArrayInDeltaT[i]/maxStepInOneCycle - 1;
		truncCyclesForGenes[i] = minimumDelayArrayInDeltaT[i] % maxStepInOneCycle;
	}

	for (i=0;i<=cycles;++i)
	{
		if (correctConcentrationValues == FALSE) return;
		startPosition += maxStepInOneCycle;
		endPosition += maxStepInOneCycle;
		if (endPosition > (sizeOfConcArray-maxStepsInOneDay)) endPosition = sizeOfConcArray-maxStepsInOneDay;
		if (startPosition > endPosition) startPosition = endPosition;
		
		//for all genes
		for (j=0;j<nrOfAllGenes;++j)
		{
			if (i>cyclesForGenes[j])
			{
				//at least one interaction is active
				executeInteractionActivePhaseForOneGene(j, startPosition, endPosition);
			}
			else
			{
				if (i<cyclesForGenes[j])
				{
					//interactions are not active
					executeInteractionFreePhaseForOneGene(j, startPosition, endPosition);
				}
				else
				{
					//at least one interaction becomes active during the cycle
					truncPosition = truncCyclesForGenes[j];
					if (truncPosition > 0)
						executeInteractionFreePhaseForOneGene(j, startPosition, startPosition + truncPosition);
					executeInteractionActivePhaseForOneGene(j, startPosition + truncPosition, endPosition);
				}
			}
		}
	}
}

void TExtGeneVector::smartExecuteInPresenceOfActiveInteractions()
{
	int i,j;
	double k;

	//random evaluation coef
	double rndEvalCoef = ((double) (rand() % 1024))/1024.0;
	double EvalCoef;

	//criteria parameters
	bool hazardCritValue = FALSE;

	//determine the cycle parameters
	int maxStepInOneCycle = minimalDelayInDeltaT;
	
	//Speedup: do not calculate unused values
	int totalSimulationSteps = sizeOfConcArray - 2 * maxStepsInOneDay +  maxObsTime + 1;
	int wholeDays;
	double smallCycles = ((double) maxStepsInOneDay)/((double) maxStepInOneCycle); 
	int cycles = totalSimulationSteps/maxStepInOneCycle;
	
	int startPosition = 0;
	int endPosition = maxStepInOneCycle;
	int truncPosition;

	for (i=0;i<nrOfAllGenes;++i)
	{
		cyclesForGenes[i] = minimumDelayArrayInDeltaT[i]/maxStepInOneCycle - 1;
		truncCyclesForGenes[i] = minimumDelayArrayInDeltaT[i] % maxStepInOneCycle;
	}

	i = 0;
	k = 0.0;
	wholeDays = 0;
	while (i<cycles)
	{
		if (correctConcentrationValues == FALSE) return;
		startPosition += maxStepInOneCycle;
		endPosition += maxStepInOneCycle;
		if (endPosition > (sizeOfConcArray-maxStepsInOneDay)) endPosition = sizeOfConcArray-maxStepsInOneDay;
		if (startPosition > endPosition) startPosition = endPosition;
		
		//for all genes
		for (j=0;j<nrOfAllGenes;++j)
		{
			if (i>cyclesForGenes[j])
			{
				//at least one interaction is active
				executeInteractionActivePhaseForOneGene(j, startPosition, endPosition);
			}
			else
			{
				if (i<cyclesForGenes[j])
				{
					//interactions are not active
					executeInteractionFreePhaseForOneGene(j, startPosition, endPosition);
				}
				else
				{
					//at least one interaction becomes active during the cycle
					truncPosition = truncCyclesForGenes[j];
					if (truncPosition > 0)
						executeInteractionFreePhaseForOneGene(j, startPosition, startPosition + truncPosition);
					executeInteractionActivePhaseForOneGene(j, startPosition + truncPosition, endPosition);
				}
			}
		}
		i++;

		if (hazardCritValue == FALSE)
		{
			k += 1.0;

			if (k > smallCycles)
			{
				++wholeDays;

				if (wholeDays == 1)
				{
					oldCritValue = -1;
					highThrCritValue = lastAcceptedCritValue * highTreshold[rungeKuttaDaysTruncated-1];
				}
				else
					highThrCritValue = lastAcceptedCritValue * lowTreshold[rungeKuttaDaysTruncated-1];

				calculateSquareDiffAndScaleFromObsData(wholeDays);
				calculateCritValue(wholeDays, ownLogGamma);

				if (wholeDays > 1)
					if (2*oldCritValue < critValue)
					{
						hazardCritValue = TRUE;
						simulatedDays = maxExperimentSizeInDays;
						correctConcentrationValues = FALSE;       //we cannot accept instable solutions
						return;
					}

				if (wholeDays > 5)
					if (lastCritValueDiff > abs(critValue/100.0))    //the consecutive difference has to be greather than a given threshold 
					{
						if (lastCritValueDiff < abs(critValue - oldCritValue))
						{
							hazardCritValue = TRUE;
							simulatedDays = maxExperimentSizeInDays;
							correctConcentrationValues = FALSE;       //we cannot accept instable solutions
							return;
						}
					}

				lastCritValueDiff = abs(critValue - oldCritValue);

				if (hazardCritValue == FALSE)
				{
					TotalCases++;
					if (critValue > highThrCritValue)
					{
						RejectedCases++;
						EvalCoef = ((double) RejectedCases)/((double) TotalCases);
						if (rndEvalCoef > EvalCoef) 
						{
							correctConcentrationValues = FALSE; 
							return;
						}
					}

					if (TotalCases > 1000000000)
					{
						TotalCases = 0;
						RejectedCases = 0;
					}

					if (abs(critValue - oldCritValue) < similarityThreshold[rungeKuttaDaysTruncated-1])
					{
						simulatedDays = wholeDays;
						return;
					}
				}
				
				k -= smallCycles;
				oldCritValue = critValue;
			}
		}
	}

	if (hazardCritValue == TRUE)
		calculateSquareDiffAndScaleFromObsData(simulatedDays);

	simulatedDays = wholeDays;
}

void TExtGeneVector::calculateSquareDiffAndScaleFromObsData(int dayOfInvestigation)
{
	int i,j,k;
	
	int posInConcArray, posInConcArray2;
	int* intPtr;
	double* doublePtr;
	double tmp, tmp2, tmp3, h1;
	scale = 0.0;
	int num = 0;

	critValue = 0.0;
	
	if (correctConcentrationValues == FALSE)
	{
		critValue = highestPossibleConcentration;
		return;
	}

	for (i=0;i<(nrOfAllGenes);++i)
	{
		//Calculate the place from where we have to read the concentration data
		posInConcArray = i;
		if (i>= nrOfGenes) posInConcArray += i - nrOfGenes; 
		posInConcArray *= sizeOfConcArray;
		//posInConcArray += sizeOfConcArray - maxStepsInOneDay;
		posInConcArray += dayOfInvestigation*maxStepsInOneDay;

		tmp2 = 0.0;
		k = arrayNrOfObs[2*i];
		if (k>0)
		{
			intPtr = obsTimePtr[2*i];
			doublePtr = obsConcPtr[2*i];
			tmp2 = 1.0;

			for (j=0;j<k;++j)
			{
				h1 = log(concArray[posInConcArray+intPtr[j]]);
				tmp2 += doublePtr[j] - h1;
				tmp = doublePtr[j] - h1 - newLogGamma;
				critValue += tmp * tmp;
			}
			tmp2 /= k;
			num++;
		}

		tmp3 = 1.0;
		k = arrayNrOfObs[2*i+1];
		if (k>0)
		{
			intPtr = obsTimePtr[2*i+1];
			doublePtr = obsConcPtr[2*i+1];
			posInConcArray2 = posInConcArray + sizeOfConcArray;
			for (j=0;j<k;++j)
			{
				h1 = log(concArray[posInConcArray2+intPtr[j]]);
				tmp3 += doublePtr[j] - h1;
				tmp = doublePtr[j] - h1 - newLogGamma;
				critValue += tmp * tmp;
			}
			tmp3 /= k;
			tmp2 = (tmp2 + tmp3)/2;
		}

		scale += tmp2;
	}

	ownLogGamma = scale/num;
}

double TExtGeneVector::calculateCritValue(int dayOfInvestigation, double LogGamma)
{
	int i,j,k;
	int posInConcArray;
	int* intPtr;
	double* doublePtr;
	double tmp;

	critValue = 0.0;

	if (correctConcentrationValues == FALSE)
	{
		critValue = highestPossibleConcentration;
		return critValue;
	}

	for (i=0;i<nrOfAllGenes;++i)
	{
		//Calculate the place from where we have to read the concentration data
		posInConcArray = i;
		if (i>= nrOfGenes) posInConcArray += i - nrOfGenes; 
		posInConcArray *= sizeOfConcArray;
		//posInConcArray += sizeOfConcArray - maxStepsInOneDay;
		posInConcArray += dayOfInvestigation * maxStepsInOneDay;

		k = arrayNrOfObs[2*i];
		if (k>0)
		{
			intPtr = obsTimePtr[2*i];
			doublePtr = obsConcPtr[2*i];

			for (j=0;j<k;++j)
			{
				tmp = doublePtr[j] - log(concArray[posInConcArray+intPtr[j]]) - LogGamma;
				critValue += tmp * tmp;
			}
		}

		k = arrayNrOfObs[2*i+1];
		if (k>0)
		{
			intPtr = obsTimePtr[2*i+1];
			doublePtr = obsConcPtr[2*i+1];
			posInConcArray += sizeOfConcArray;
		
			for (j=0;j<k;++j)
			{
				tmp = doublePtr[j] - log(concArray[posInConcArray+intPtr[j]]) - LogGamma;
				critValue += tmp * tmp;
			}
		}
	}
	return critValue;
}
			

//****************************************************************
//Debug helper methods
//****************************************************************

int TExtGeneVector::runRungeKuttaUsingDebug(int length)
{
	long steps = wallClock.numTimeSteps * length - 1;
	errors = 0;

	int i,j,k;
	double oldConc_mRNA, newConc_mRNA, oldConc_PreRNA, newConc_PreRNA;
	double increasedConcProportion;

	if ((concArray != NULL) && (correctConcentrationValues != FALSE))
	{
		for(i=0; i<steps; ++i)
		{
			updateOneTimestepRungeKutta();
			j = 0;
			k = 0;

			for(geneIt=allGenes.begin(); geneIt!=allGenes.end(); ++geneIt)
			{
				oldConc_mRNA = (*geneIt)->mRNAConcentrations->current();
				oldConc_PreRNA = (*geneIt)->getPreRNAConcentration();
			
				if (oldConc_PreRNA == -1.00)
				{
					newConc_mRNA = concArray[j*sizeOfConcArray+maxStepsInOneDay+i+1];
					newConc_PreRNA = -1.00;
					j++;
				}
				else
				{
					newConc_mRNA = concArray[(2*k+nrOfGenes)*sizeOfConcArray+maxStepsInOneDay+i+1];
					newConc_PreRNA = concArray[(2*k+1+nrOfGenes)*sizeOfConcArray+maxStepsInOneDay+i+1];
					k++;
				}
			
				increasedConcProportion = oldConc_mRNA/newConc_mRNA;
				if (increasedConcProportion < 1) increasedConcProportion = 1/increasedConcProportion;
				if ((increasedConcProportion < 1) || (increasedConcProportion > debugPropThreshold))
				{
					errors++;
				}
			
				increasedConcProportion = oldConc_PreRNA/newConc_PreRNA;
				if (increasedConcProportion < 1) increasedConcProportion = 1/increasedConcProportion;
				if ((increasedConcProportion < 1) || (increasedConcProportion > debugPropThreshold))
				{
					errors++;
				}
			}
		}
	}

	return errors;
}

double TExtGeneVector::getLogLikelihoodTermForHastingsAllDaysUsingDebug(double & thisLogGamma)
{
	resetRungeKutta();
	runRungeKuttaUsingDebug(daysDiff[0]);
	return getLogLikelihoodTermForHastings(thisLogGamma);
}

bool TExtGeneVector::checkStartingValuesUsingDebug()
{
	//This function contains the old checkStartingValues function
	//extended with various debug ficilities in order to test 
	//the correctness of the fast method
	
	//This function will calculate the posterior density and accept or reject the proposed starting values
	//make Runge Kutta run
	getLogLikelihoodTermForHastingsAllDaysUsingDebug(logGamma);
	calculateSquareDiffAndScaleFromObsData(maxExperimentSizeInDays);
	
	//fit good starting Gamma as median scaling across all data and genes
	newLogGamma = getScaleEstimate();
	
	//calculate posterior density
	newLogPriorDens = paramVec.logPriorDensityTermforHastings();
	newLogPosteriorDens = minusOneOverTwoSimgaSquared * getLogLikelihoodTermForHastingsAllDaysOnlyGammaUpdate(newLogGamma) + newLogPriorDens;
	
	ownLogPriorDens = paramVec.logPriorDensityTermforHastings();
	ownLogPosteriorDens = minusOneOverTwoSimgaSquared * calculateCritValue(maxExperimentSizeInDays, ownLogGamma) + newLogPriorDens;
	
	//accept or reject
	if(newLogPosteriorDens < oldLogPosteriorDens)
	{
		//reset
		paramVec.resetOldMCMC();	
	} 
	else 
	{
		highThrCritValue = critValue * highTreshold[0];
		++numAccepted;
		logGamma = newLogGamma;
		oldLogPosteriorDens = newLogPosteriorDens;
		oldLogPriorDens = newLogPriorDens;
	}

	return false;
}

//****************************************************************
//Controlling methods
//****************************************************************

void TExtGeneVector::setAndCheckStartingValues(TRandomGenerator* randomGenerator)
{
	correctConcentrationValues = TRUE;
	sampleRandomParameterValuesFromPrior(randomGenerator);

	//Select the invoked processing method
	if (executionMode == 0) checkStartingValues();  //Run the old method
	else
	{
		if (executionMode == 1)
		{

			//Execute the faster method if it is possible
			if (isFirstTimeExecuted == TRUE)
			{
				speedRoutineAllowed = initializeAdditionalFields();
				isFirstTimeExecuted = FALSE;
				critValue = (oldLogPosteriorDens - oldLogPriorDens)/minusOneOverTwoSimgaSquared;
				//lastAcceptedCritValue = critValue;
			}
			if (speedRoutineAllowed)
			{
				prepareArraysForCalculations();
				executeInteractionFreePhase();
				
				if (rungeKuttaDaysTruncated > 0)
				{
					smartExecuteInPresenceOfActiveInteractions();
				}
				else
				{
					//Now only 0 and 1 are considered valid values, so any other value than 1 is considered 0
					simulatedDays = maxExperimentSizeInDays;
					executeInPresenceOfActiveInteractions();
					calculateSquareDiffAndScaleFromObsData(simulatedDays);
					
				}

				//calculate posterior density
				ownLogPriorDens = paramVec.logPriorDensityTermforHastings();
				ownLogPosteriorDens = minusOneOverTwoSimgaSquared * calculateCritValue(simulatedDays, ownLogGamma) + ownLogPriorDens;

				//accept or reject
				if(ownLogPosteriorDens < oldLogPosteriorDens)
				{
					//reset
					paramVec.resetOldMCMC();	
				} 
				else 
				{
					lastAcceptedCritValue = critValue;
					++numAccepted;
					logGamma = ownLogGamma;
					newLogGamma = ownLogGamma;
					oldLogPosteriorDens = ownLogPosteriorDens;
					oldLogPriorDens = ownLogPriorDens;
					newLogPosteriorDens = ownLogPosteriorDens;
					newLogPriorDens = ownLogPriorDens;
				}
			}
			else
				checkStartingValues();  //Run the old method
		}
		else
		{
			//Debugging mode for result comparisor between the two methods 
			if (isFirstTimeExecuted == TRUE)
			{
				speedRoutineAllowed = initializeAdditionalFields();
				isFirstTimeExecuted = FALSE;
			}
			if (speedRoutineAllowed)
			{
				prepareArraysForCalculations();
				executeInteractionFreePhase();
				executeInPresenceOfActiveInteractions();
				checkStartingValuesUsingDebug();  //as both methods are executed the results can be compared
			}
		}
	}
}

void TExtGeneVector::onlyCheckStartingValues()
{
	correctConcentrationValues = TRUE;
	
	//Select the invoked processing method
	if (executionMode == 0) checkStartingValues();  //Run the old method
	else
	{
		if (executionMode == 1)
		{
			//Execute the faster method if it is possible
			if (isFirstTimeExecuted == TRUE)
			{
				speedRoutineAllowed = initializeAdditionalFields();
				isFirstTimeExecuted = FALSE;
				critValue = (oldLogPosteriorDens - oldLogPriorDens)/minusOneOverTwoSimgaSquared;
				lastAcceptedCritValue = critValue;
			}
			if (speedRoutineAllowed)
			{
				prepareArraysForCalculations();
				executeInteractionFreePhase();
				
				if (rungeKuttaDaysTruncated > 0)
				{
					smartExecuteInPresenceOfActiveInteractions();
				}
				else
				{
					//Now only 0 and 1 are considered valid values, so any other value than 1 is considered 0
					simulatedDays = maxExperimentSizeInDays;
					executeInPresenceOfActiveInteractions();
					calculateSquareDiffAndScaleFromObsData(simulatedDays);
				}

				//calculate posterior density
				ownLogPriorDens = paramVec.logPriorDensityTermforHastings();
				ownLogPosteriorDens = minusOneOverTwoSimgaSquared * calculateCritValue(simulatedDays, ownLogGamma) + ownLogPriorDens;

				//accept or reject
				if(ownLogPosteriorDens < oldLogPosteriorDens)
				{
					//reset
					paramVec.resetOldMCMC();	
				} 
				else 
				{
					lastAcceptedCritValue = critValue;
					++numAccepted;
					logGamma = ownLogGamma;
					newLogGamma = ownLogGamma;
					oldLogPosteriorDens = ownLogPosteriorDens;
					oldLogPriorDens = ownLogPriorDens;
					newLogPosteriorDens = ownLogPosteriorDens;
					newLogPriorDens = ownLogPriorDens;
				}
			}
			else
				checkStartingValues();  //Run the old method
		}
		else
		{
			//Debugging mode for result comparisor between the two methods 
			if (isFirstTimeExecuted == TRUE)
			{
				speedRoutineAllowed = initializeAdditionalFields();
				isFirstTimeExecuted = FALSE;
			}
			if (speedRoutineAllowed)
			{
				prepareArraysForCalculations();
				executeInteractionFreePhase();
				executeInPresenceOfActiveInteractions();
				checkStartingValuesUsingDebug();  //as both methods are executed the results can be compared
			}
		}
	}
}

void TExtGeneVector::performOneMCMCStep()
{
	int p;
	//Select the invoked processing method
	if (executionMode == 0) performMCMCStep();  //Run the old method
	else
	{
		if (executionMode == 1)
		{
			//per iteration, update all parameters once
			bool oneAccepted = false;

			for(p=0; p<numParamsToUpdate; ++p)
			{

				correctConcentrationValues = TRUE;
				paramVec.updateMCMCOne(normalRand[p]);
				prepareArraysForCalculations();
				executeInteractionFreePhase();

				if (rungeKuttaDaysTruncated > 0)
				{
					smartExecuteInPresenceOfActiveInteractions();
					
					//calculate posterior density
					ownLogPriorDens = paramVec.logPriorDensityTermforHastings();
					ownLogPosteriorDens = minusOneOverTwoSimgaSquared * calculateCritValue(simulatedDays, logGamma) + ownLogPriorDens;
				}
				else
				{
					//Now only 0 and 1 are considered valid values, so any other value than 1 is considered 0
					simulatedDays = maxExperimentSizeInDays;
					executeInPresenceOfActiveInteractions();
					calculateSquareDiffAndScaleFromObsData(simulatedDays);
				
					//calculate posterior density
					ownLogPriorDens = paramVec.logPriorDensityTermforHastings();
					ownLogPosteriorDens = minusOneOverTwoSimgaSquared * calculateCritValue(simulatedDays, logGamma) + ownLogPriorDens;
				}

				hastingsLog = heat*(ownLogPosteriorDens - oldLogPosteriorDens);

				//std::cout << "heat = " << heat << ", h_log = " << hastingsLog << ", h = " << exp(hastingsLog) << std::endl;

				if(unifRand[p] < exp(hastingsLog)){
					lastAcceptedCritValue = critValue;
					++numAccepted;
					oldLogPosteriorDens = ownLogPosteriorDens;
					oldLogPriorDens = ownLogPriorDens;
					
					//update Gamma if we use only one day, as there is no need to rerun Runge Kutta!
					//but only once per iteration
					if(!oneAccepted)
					{
						++numTried;
						newLogGamma = logGamma + normalRand[numParamsToUpdate] * propGamma;
						//TODO: check only Gamma!
						//newLogPosteriorDens = minusOneOverTwoSimgaSquared * getLogLikelihoodTermForHastingsAllDays(newLogGamma) + oldLogPriorDens;
					
						calculateSquareDiffAndScaleFromObsData(simulatedDays);
					/*
						if (rungeKuttaDaysTruncated > 0)
							calculateSquareDiffAndScaleFromObsData(simulatedDays);
						else
							calculateSquareDiffAndScaleFromObsData(maxExperimentSizeInDays);
							*/
						newLogPosteriorDens = minusOneOverTwoSimgaSquared * critValue + oldLogPriorDens;

						hastingsLog = heat*(newLogPosteriorDens - oldLogPosteriorDens);
						if(unifRand[numParamsToUpdate] < exp(hastingsLog)){
							lastAcceptedCritValue = critValue;
							oldLogPosteriorDens = newLogPosteriorDens;
							++numAccepted;
							logGamma = newLogGamma;
							ownLogGamma = newLogGamma;
						}
					}
					oneAccepted = true;
				} else paramVec.resetOldMCMC();		
			}
		}
		numTried += numParamsToUpdate;
	}
}

