#ifndef __SSDE_MCMC__
#define __SSDE_MCMC__

	#include <time.h>
	#include <stdio.h>

	//created due Win platform compatibility SZS
	#if defined _WIN32
	  // Windows stuff
		#include "linux_time.h"
	#else
	  // Other stuff
	  #include <sys/time.h>
		#include <unistd.h>
	#endif

	int old_main(int argc, char* argv[]);
	
#endif
