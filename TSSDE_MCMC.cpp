/*
 * TSSDE_MCMC.cpp
 *
 *  Created on: Apr 22, 2014
 *      Author: wegmannd
 */


#include "TSSDE_MCMC.h"

//SZS
//modified due Win platform compatibility
#if defined _WIN32
	// Windows stuff
	#include <omp.h>
	double round(double number){
		return number < 0.0 ? ceil(number - 0.5) : floor(number + 0.5);}
#endif
//END_SZS

TSSDE_MCMC::TSSDE_MCMC(TLog* Logfile, TParameters* MyParameters){
	logfile=Logfile;
	myParameters=MyParameters;

	//initialize random generator
	logfile->listFlush("Initializing random generator ...");
	if(myParameters->parameterExists("fixedSeed")){
		randomGenerator=new TRandomGenerator(myParameters->getParameterLong("fixedSeed"), true);
	} else if(myParameters->parameterExists("addToSeed")){
		randomGenerator=new TRandomGenerator(myParameters->getParameterLong("addToSeed"), false);
	} else randomGenerator=new TRandomGenerator();
	logfile->flush(" done with seed ");
	logfile->flush(randomGenerator->usedSeed);
	logfile->write("!");

	//initialize other variables
	numCoresToUse = MyParameters->getParameterIntWithDefault("numCores", 1);
	if(numCoresToUse > 1) logfile->list("Using " + toString(numCoresToUse) + " cores in parallel.");
}

//functions
void TSSDE_MCMC::performMCMCInference(){
	//read gene network

	//SZS
	TExtGeneVector* coldChain = new TExtGeneVector(logfile, myParameters);
	coldChain->initializeAdditionalParameters(myParameters);
	//END_SZS
	coldChain->initializeMCMC(myParameters);

	//open output file
	std::string outname = myParameters->getParameterStringWithDefault("outname", "MCMCrun");
	std::string filename = outname + "_MCMC.txt";

	logfile->list("Writing resulting MCMC chain to '", filename, "'");

	std::ofstream mcmcOut(filename.c_str());
	if(!mcmcOut) throw "Failed to one file '" + filename + "'!";
	coldChain->writeMCMCHeader(mcmcOut);

	//prepare MCMC: make first run
	logfile->listFlush("Perform initial Runge-Kutta run ...");
	coldChain->prepareMCMCRun();
	logfile->write(" done!");
	logfile->listFlush("Finding good starting value for the scaling factor Gamma ...");
	logfile->write(" done!");
	logfile->conclude("Setting initial log(Gamma) = " + toString(coldChain->getCurLogGamma()));
	logfile->conclude("Initial log posterior density = " + toString(coldChain->getCurLogPosteriorDensity()));
	logfile->endIndent();

	//read mcmc parameters
	logfile->startIndent("Running and MCMC with the following settings:");
	//chain length
	long length = myParameters->getParameterLongWithDefault("chainLength",1000);
	logfile->list("chain will be of length ", length);

	//burnin
	long burnin = myParameters->getParameterLongWithDefault("burnin",0);
	logfile->list("additional ", burnin, " iterations will be used as burnin");

	//sampling
	int sampling = myParameters->getParameterLongWithDefault("sampling", 1);
	if(sampling < 1) sampling = 1;
	if(sampling==1) logfile->list("will output every iteration");
	else if(sampling==2) logfile->list("will output every second iteration");
	else if(sampling==3) logfile->list("will output every third iteration");
	else logfile->list("will output every ", sampling, "th iteration");

	//heated chains
	int numChains = 1;
	bool tempering = false;
	TExtGeneVector** geneObjects;
	std::ofstream* heatedOut;
	bool printHeated = false;

	if(myParameters->parameterExists("numChains")){
		numChains = myParameters->getParameterInt("numChains");
		if(numChains < 1) throw "Number of chains has to be >= 1!";
		if(numChains > 1){
			tempering = true;
			geneObjects = new TExtGeneVector*[numChains];
			printHeated = myParameters->parameterExists("printHeated");
			if(printHeated){
				heatedOut = new std::ofstream[numChains];
			}

			geneObjects[0] = coldChain;
			logfile->startIndent("Will run " + toString(numChains) + " MCMC chains in parallel:");
			tempering = true;
			double deltaT = myParameters->getParameterDoubleWithDefault("deltaT", 0.1);
			logfile->list("Using deltaT = " + toString(deltaT));
			logfile->startIndent("Initializing " + toString(numChains-1) + " heated chains:");
			std::string temp = "1.0";
			for(int i=1; i<numChains; ++i){
				geneObjects[i] = new TExtGeneVector(coldChain);
				double heat = 1.0 / (1.0 + deltaT*(i));
				geneObjects[i]->setHeat(heat);
				geneObjects[i]->sampleRandomParameterValuesFromPrior(randomGenerator);
				temp += ", " + toString(heat);

				if(printHeated){
					filename = outname + "_MCMC_heatedChain" + toString(i) + ".txt";
					logfile->list("Writing heated chains to '", filename, "'");
					heatedOut[i].open(filename.c_str());
					if(!heatedOut[i]) throw "Failed to open file '" + filename + "'!";
					geneObjects[i]->writeMCMCHeader(heatedOut[i]);
				}
			}
			logfile->list("Resulting temperatures were " + temp);
			logfile->endIndent();
		}
	}

	//Find starting values
	int prog = 0;
	int oldProg = 0;
	int numStartingValuesToTry = myParameters->getParameterIntWithDefault("startingVals", 1);
	if(numStartingValuesToTry < 1) throw "Number of starting values has to be > 0!";
	if(numStartingValuesToTry > 0){
		logfile->listFlush("Search best among " + toString(numStartingValuesToTry) + " starting positions ... (0%)");

		for(int i=0; i<numStartingValuesToTry; ++i){
			if(numChains > 1){
				//store random numbers
				for(int c=0; c<numChains; ++c)
					geneObjects[c]->sampleRandomParameterValuesFromPrior(randomGenerator);

				//run all chains
				#pragma omp parallel num_threads(numCoresToUse)
				{
					#pragma omp for
					for(int c=0; c<numChains; ++c)
						geneObjects[c]->onlyCheckStartingValues();
				}

			} else {
				coldChain->setAndCheckStartingValues(randomGenerator);
			}

			//report progress
			prog = i / (double) numStartingValuesToTry * 100;
			if(prog > oldProg){
				oldProg = prog;
				logfile->listOverFlush("Search best among " + toString(numStartingValuesToTry) + " starting positions ... (" + toString(prog) + "%)");
			}
		}
		logfile->overList("Search best among " + toString(numStartingValuesToTry) + " starting positions ... done!   ");

		//report success rate
		int numAccepted = 0;
		if(tempering){
			for(int c=0; c<numChains; ++c){
				numAccepted += geneObjects[c]->getNumAccepted();
				geneObjects[c]->resetNumAccepted();
			}
		} else {
			numAccepted = coldChain->getNumAccepted();
			coldChain->resetNumAccepted();
		}

		logfile->conclude(toString(numAccepted) + " (" + toString(round(100 * (double) numAccepted / (double) (numStartingValuesToTry * numChains))) + "%) proposals accepted");
		//report starting LL of cold chain
		logfile->conclude("log posterior density at starting position = " + toString(coldChain->getCurLogPosteriorDensity()));
	}

	//output starting values to file
	coldChain->writeMCMCOutput(mcmcOut);
	if(printHeated){
		for(int i=1; i<numChains; ++i){
			geneObjects[i]->writeMCMCOutput(heatedOut[i]);
		}
	}

	//declare runtime variables
	int lastPrinted = 1;
	length = length + burnin;
	logfile->listFlush("Running MCMC for " + toString(length) + " iterations ... (0%)");
	prog=0;
	oldProg=0;

	//SZS
	//modified due Win platform compatibility
	#if defined _WIN32
	  // Windows stuff
		InitTimeMeasFrequency();
		struct timeval2 start, end;	
	#else
	  // Other stuff
		struct timeval start, end;
	#endif
	//END_SZS

	gettimeofday(&start, NULL);
	
	double acceptanceRate;
  double h_log_swap;

	//SZS
  //TGeneVector* tmpChain;
	TExtGeneVector* tmpChain;
	//END_SZS

  long triedSwaps = 0;
  long acceptedSwaps = 0;
  double swapRate;

  //run MCMC
  for(long iteration=0; iteration<length; ++iteration){
		if(tempering){
			//store random numbers
			for(int c=0; c<numChains; ++c)
				geneObjects[c]->prepareMCMCStep(randomGenerator);

			//SZS
			//run all chains
			#pragma omp parallel num_threads(numCoresToUse)
			{
				#pragma omp for
				for(int c=0; c<numChains; ++c)
					geneObjects[c]->performOneMCMCStep();
			}
			//END_SZS

			//swap between chains
			for(int c=0; c<(numChains-1); ++c){
				++triedSwaps;
				//swap with chain of next higher temperature

				h_log_swap = (geneObjects[c+1]->getHeat() - geneObjects[c]->getHeat()) * (geneObjects[c]->getCurLogPosteriorDensityForHastings() - geneObjects[c+1]->getCurLogPosteriorDensityForHastings());
				
				//SZS
				//condition added because otherwise a lot of unnecessary swaps are performed 
				if (h_log_swap != 0.0)
				{
					if(randomGenerator->getRand() < exp(h_log_swap)){
						++acceptedSwaps;
						//accept swap: switch heats!
						geneObjects[c]->swap(geneObjects[c+1]);

						//switch order
						tmpChain = geneObjects[c];
						geneObjects[c] = geneObjects[c+1];
						geneObjects[c+1] = tmpChain;

						//change chain from which to sample
						if(c==0) coldChain = (TExtGeneVector*) geneObjects[0];
					}
				}
				//END_SZS
			}
		} else {
			coldChain->prepareMCMCStep(randomGenerator);

			//SZS
			//coldChain->performMCMCStep();
			coldChain->performOneMCMCStep();
			//END_SZS
		}

		//progress
		prog = 1000 * (double) iteration / (double) length;
		if(prog>oldProg){
			oldProg=prog;
			acceptanceRate = (int) (1000 * coldChain->getAcceptaneRate()) / 10.0;
			if(tempering){
				swapRate = (int) (1000 * (double) acceptedSwaps / (double) triedSwaps) / 10.0;
				logfile->listOverFlush("Running MCMC for " + toString(length) + " iterations ... (" + toString(prog/10.0) + "%, acceptance rate = " + toString(acceptanceRate) + "%, swap rate = " + toString(swapRate) + "%)  ");
			} else {
				logfile->listOverFlush("Running MCMC for " + toString(length) + " iterations ... (" + toString(prog/10.0) + "%, acceptance rate = " + toString(acceptanceRate) + "%)  ");
			}
		}

		//output
		if(iteration>burnin && lastPrinted>=sampling){
			coldChain->writeMCMCOutput(mcmcOut);
			lastPrinted=1;
			if(printHeated){
				for(int i=1; i<numChains; ++i){
					geneObjects[i]->writeMCMCOutput(heatedOut[i]);
				}
			}
		} else ++lastPrinted;

	}
	logfile->overList("running MCMC for a total of ", length, " iterations ... done!                                      ");
	acceptanceRate = (int) (1000 * coldChain->getAcceptaneRate()) / 10.0;
	logfile->conclude("Acceptance rate was " + toString(acceptanceRate) + "%");
	if(tempering){
		swapRate = (int) (1000 * (double) acceptedSwaps / (double) triedSwaps) / 10.0;
		logfile->conclude("Swap rate was " + toString(swapRate) + "%");
	}
	gettimeofday(&end, NULL);
	double runtime=(double) (end.tv_sec  - start.tv_sec) / 60.0;
	logfile->conclude("total run time was ", runtime, "min");

	//clean up
	mcmcOut.close();
	if(printHeated){
		for(int i=1; i<numChains; ++i){
			heatedOut[i].close();
		}
		delete[] heatedOut;
	}
	if(tempering){
		for(int i=0; i<numChains; ++i){
			delete geneObjects[i];
		}
		delete[] geneObjects;
	} else delete coldChain;

}

void TSSDE_MCMC::calcLikelihood(){
	logfile->startIndent("Calculating Log-Likelihood:");
	double logGamma = myParameters->getParameterDoubleWithDefault("logGamma", 0.0);
	logfile->list("Using logGamma = " + toString(logGamma));

	//read gene network
	TGeneVector* genes = new TGeneVector(logfile, myParameters);
	if(genes->checkParameters()){
		//read data
		genes->readData(myParameters->getParameterString("data"));

		//calc log likelihood
		genes->calcLogLikelihood(logGamma);
	} else {
		logfile->warning("Parameter combination violates rules!");
		logfile->conclude("LL = nan");
	}
	logfile->endIndent();
}

void TSSDE_MCMC::simulate(){
	//read num simulations to perform
	int rep = myParameters->getParameterInt("rep", false);
	if(rep < 1) rep = 1;
	logfile->list("Generating " + toString(rep) + " simulations.");

	//read gene network
	TGeneVector genes(logfile, myParameters);
	genes.initializeSimulations(myParameters);

	//open output file
	std::string outname = myParameters->getParameterStringWithDefault("outname", "SSDE");
	std::string paramFile = outname + "_simulations_parameters.txt";
	genes.openSimulationOutput(paramFile);

	//generate simulations
	logfile->startIndent("Generating " + toString(rep) + " simulations:");
	for(int i=0; i<rep; ++i){
		logfile->startIndent("Simulation " + toString(i+1) + ":");
		genes.simulateData(i+1, outname, randomGenerator);
		logfile->endIndent();
	}

	logfile->endIndent();

}
